package com.rezgateway.automation.builder.docBuilder;

import java.io.File;
import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

public class DocumentBuild {
	public Document docBuilder(String filepath) throws JDOMException, IOException{
		Document doc = null;
		try {
			File file = new File(filepath);
			SAXBuilder builder = new SAXBuilder();
			doc = builder.build(file);	
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception : " + e.getMessage());
		}
		return doc;
	}
}
