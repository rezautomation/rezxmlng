package com.rezgateway.automation.pojo;

public class CancellationResponse {

	private String                      ScenarioID									= null;
	private String 						CancellationResponseStatus					= null;
	private String 						TourOperatorOrderNumber 					= null;
	private String 						CancellationNo 								= null;
	private String 						CanellationFee 								= null;
	private String 						ErrorCode 									= null;
	private String 						ErrorDescription 							= null;
	/**
	 * @return the tourOperatorOrderNumber
	 */
	public String getTourOperatorOrderNumber() {
		return TourOperatorOrderNumber;
	}
	/**
	 * @return the cancellationNo
	 */
	public String getCancellationNo() {
		return CancellationNo;
	}
	/**
	 * @return the canellationFee
	 */
	public String getCanellationFee() {
		return CanellationFee;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return ErrorCode;
	}
	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return ErrorDescription;
	}
	/**
	 * @param tourOperatorOrderNumber the tourOperatorOrderNumber to set
	 */
	public void setTourOperatorOrderNumber(String tourOperatorOrderNumber) {
		TourOperatorOrderNumber = tourOperatorOrderNumber;
	}
	/**
	 * @param cancellationNo the cancellationNo to set
	 */
	public void setCancellationNo(String cancellationNo) {
		CancellationNo = cancellationNo;
	}
	/**
	 * @param canellationFee the canellationFee to set
	 */
	public void setCanellationFee(String canellationFee) {
		CanellationFee = canellationFee;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}
	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		ErrorDescription = errorDescription;
	}
	/**
	 * @return the cancellationResponseStatus
	 */
	public String getCancellationResponseStatus() {
		return CancellationResponseStatus;
	}
	/**
	 * @param cancellationResponseStatus the cancellationResponseStatus to set
	 */
	public void setCancellationResponseStatus(String cancellationResponseStatus) {
		CancellationResponseStatus = cancellationResponseStatus;
	}
	/**
	 * @return the scenarioID
	 */
	public String getScenarioID() {
		return ScenarioID;
	}
	/**
	 * @param scenarioID the scenarioID to set
	 */
	public void setScenarioID(String scenarioID) {
		ScenarioID = scenarioID;
	}
	
}
