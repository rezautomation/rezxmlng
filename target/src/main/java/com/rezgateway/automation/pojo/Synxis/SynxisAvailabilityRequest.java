package com.rezgateway.automation.pojo.Synxis;

import java.util.ArrayList;

public class SynxisAvailabilityRequest {
	private String 			username 		= null;
	private String 			password 		= null;
	private String 			checkin 		= null;
	private String 			checkout 		= null;	
	private int 			adultCount 		= 0;
	private int 			childCount 		= 0;
	private int 			roomCount		= 0;
	private String 			hotelcode 		= null;

	private String			echoToken		= null;
	private ArrayList<String> rateplan 		= null;
	
	public String getUsername(){
		return username;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getCheckin(){
		return checkin;
	}
	
	public void setCheckin(String checkin){
		this.checkin = checkin;
	}
	
	public String getCheckout(){
		return checkout;
	}
	
	public void setCheckout(String checkout){
		this.checkout = checkout;
	}
	
	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}
	
	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}
	
	public int getRoomCount() {
		return roomCount;
	}

	public void setRoomCount(int roomCount) {
		this.roomCount = roomCount;
	}

	public String getHotelcode() {
		return hotelcode;
	}

	public void setHotelcode(String hotelcode) {
		this.hotelcode = hotelcode;
	}
	
	public String getEchoToken() {
		return echoToken;
	}

	public void setEchoToken(String echoToken) {
		this.echoToken = echoToken;
	}
	
	
	public ArrayList<String> getRateplan() {
		return rateplan;
	}

	public void setRateplan(ArrayList<String> rateplan) {
		this.rateplan = rateplan;
	}

}
