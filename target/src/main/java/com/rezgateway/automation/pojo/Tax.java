package com.rezgateway.automation.pojo;

public class Tax {
	
	private String TaxName   = "N/A";
	private double TaxAmount = 0.0;
	/**
	 * @return the taxName
	 */
	public String getTaxName() {
		return TaxName;
	}
	/**
	 * @param taxName the taxName to set
	 */
	public void setTaxName(String taxName) {
		TaxName = taxName;
	}
	/**
	 * @return the taxAmount
	 */
	public Double getTaxAmount() {
		return TaxAmount;
	}
	/**
	 * @param taxAmount the taxAmount to set
	 */
	public void setTaxAmount(Double taxAmount) {
		TaxAmount = taxAmount;
	}
	

}
