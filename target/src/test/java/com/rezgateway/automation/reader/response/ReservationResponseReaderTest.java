package com.rezgateway.automation.reader.response;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.rezgateway.automation.pojo.ReservationResponse;

public class ReservationResponseReaderTest {

	@Before
	public void setup(){
		
		System.out.println("Test is Started");
	}
	
	@Test
	public void testSuiteNormalResponse() throws IOException, SAXException, Exception{
		
		  	File fXmlFile = new File("resources/SampleResResponse.xml");
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		    Document doc = dBuilder.parse(fXmlFile);
		    TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();
	        DOMSource source = new DOMSource(doc);
	        StringWriter strWriter = new StringWriter();
	        StreamResult result = new StreamResult(strWriter);
	        transformer.transform(source, result);
	        String outString = strWriter.getBuffer().toString();
	        ReservationResponseReader avaRes = new ReservationResponseReader();
			ReservationResponse readResponsObj =  avaRes.getResponse(outString);
			System.out.println(readResponsObj.getReferenceno().trim());
			
			avaRes.buildResponse("resources/Sample_Build_CNX_Response.xml", outString);
		
	}
	@Test
	public void testSuiteWithErrorResponse() throws IOException, SAXException, Exception{
		
		  	File fXmlFile = new File("resources/SampleResResponseWithError.xml");
		    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		    Document doc = dBuilder.parse(fXmlFile);
		    TransformerFactory transformerFactory = TransformerFactory.newInstance();
	        Transformer transformer = transformerFactory.newTransformer();
	        DOMSource source = new DOMSource(doc);
	        StringWriter strWriter = new StringWriter();
	        StreamResult result = new StreamResult(strWriter);
	        transformer.transform(source, result);
	        String outString = strWriter.getBuffer().toString();
	        ReservationResponseReader avaRes = new ReservationResponseReader();
			ReservationResponse readResponsObj =  avaRes.getResponse(outString);
			System.out.println(readResponsObj.getErrorDescription().trim());
			
			avaRes.buildResponse("resources/Sample_Build_CNX_Response.xml", outString);
		
	}
	
	@After
	public void afterTest(){
		
		System.out.println("Test is Done");
	}
	
}
