package com.rezgateway.automation.reader.synxis;

import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.testng.annotations.Test;

import com.rezgateway.automation.builder.docBuilder.DocumentBuild;
import com.rezgateway.automation.pojo.Synxis.SynxisAvailabilityRequest;
import com.rezgateway.automation.reader.Synxis.SynxisAvailabilityRequestReader;

public class SynxisAvailabilityRequestReaderTest {
  @Test
  public void readReqData() throws JDOMException, IOException {

		String filepath = "/home/harshani/Desktop/bonotel_automation/2CH 1 CH 2Room req.xml";
//		String filepath = "/home/harshani/Desktop/bonotel_automation/synxis avail with child req.xml";
		
		DocumentBuild builder = new DocumentBuild();
		Document doc = builder.docBuilder(filepath);
		
		SynxisAvailabilityRequestReader reader = new SynxisAvailabilityRequestReader();
		SynxisAvailabilityRequest req = new SynxisAvailabilityRequest();
		req = reader.RequestReader(doc);
		
		System.out.println("Username \t: " + req.getUsername());
		System.out.println("Password \t: " + req.getPassword());
		System.out.println("Echo token \t: " + req.getEchoToken());
		System.out.println("CheckIn \t: " + req.getCheckin());
		System.out.println("CheckOut \t: " + req.getCheckout());
		System.out.println("rate plan \t: " + req.getRateplan());
		System.out.println("Room quantity \t: " + req.getRoomCount());
		System.out.println("Adult count \t: " + req.getAdultCount());
		System.out.println("Child count \t: " + req.getChildCount());	
	  
  }
}
