/**
 * 
 */
package com.rezgateway.automation.reader.response;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.reader.response.SynxisReservationResponseReader;
import com.rezgateway.automation.pojo.Occupancy;
import com.rezgateway.automation.pojo.SynxisReservationResponse;
import com.rezgateway.automation.utills.*;

/**
 * @author Dinethra
 *
 */
public class SynxisReservationResponseReaderTest {
	
	static Logger logger;

	@Before
	public void setup(){
		System.out.println("Test Started");
	}
	
	public SynxisReservationResponseReaderTest(){
		logger = Logger.getLogger(SynxisReservationResponseReaderTest.class);
	}

	 @Test
		public void synxisSynxisReservationResponseReadData() throws JDOMException, IOException{
			
//			String filepath = "resources/SynxisResInitiateResponse.xml";
			String filepath = "resources/SynxisResCommitResponse.xml";
			DocumentBuilder builder = new DocumentBuilder();
			Document doc = builder.docBuilder(filepath);
			
			SynxisReservationResponseReader srrReader = new SynxisReservationResponseReader();
			SynxisReservationResponse srr = new SynxisReservationResponse();
			
			try {
				srr = srrReader.RequestReader(doc);
			} catch (Exception e) {
				logger.fatal("Data not read properly" + e);
			}
			
			if(srr.getResResponseType().equalsIgnoreCase("Pending"))
				System.out.println("Synxis Reservation Initiate Response \n");
			else if(srr.getResResponseType().equalsIgnoreCase("Committed"))
				System.out.println("Synxis Reservation Commit Response \n");
			
			System.out.println("Reservation response type : \t" + srr.getResResponseType());		
			System.out.println("Supplier ID : \t" + srr.getSupplierID());
			System.out.println("ID_Context : \t" + srr.getSidContext());
			System.out.println("Gurantee Code : \t" + srr.getGuranteeCode());
			System.out.println("Total Amount Before Tax : \t"+srr.getTotalBeforeTax());
			System.out.println("Total Amount After Tax : \t"+srr.getTotalAfterTax());
			System.out.println("Currency Code : \t"+srr.getCurrencyCode());
			System.out.println("Room Type Code : \t"+srr.getRoomTypeCode());
			System.out.println("No. of Units : \t"+srr.getNumberOfUnits());
			System.out.println("Room Name : \t"+srr.getRoomDesciption());
			System.out.println("Room Max Occupancy : \t"+srr.getRoomMaxOccupancy());
			System.out.println("Rate Plan Code : \t"+srr.getRatePlanCode());
			System.out.println("Rate Plan Name : \t"+srr.getRatePlanName());		
			System.out.println("Policy Code : \t" + srr.getsCancelPolicy().getPolicyCode());
			System.out.println("Policy Description : \t" + srr.getsCancelPolicy().getPenltyDesc());
			System.out.println("Amount/No. of Nights/Percentage : \t" + srr.getsCancelPolicy().getAmount());
			System.out.println("Tax Inclusive : \t" + srr.getsCancelPolicy().getIsTaxInclusive());
			System.out.println("Offset Time Unit : \t" + srr.getsCancelPolicy().getOffsetTimeUnit());
			System.out.println("Offset Unit Multiplier : \t" + srr.getsCancelPolicy().getOffsetUnitMultiplier());
			System.out.println("Offset Drop Time : \t" + srr.getsCancelPolicy().getOffsetDropTime());
			System.out.println("Base Amount Before Tax : \t"+srr.getBaseAmountBeforeTax());
			System.out.println("Base Amount After Tax : \t"+srr.getBaseAmountAfterTax());
			System.out.println("Discount Amount Before Tax : \t"+srr.getDiscountAmountBeforeTax());
			System.out.println("Discount Amount After Tax : \t"+srr.getDiscountAmountAfterTax());
			System.out.println("Fee : \t"+srr.getFeeAmount());
			System.out.println("Tax : \t"+srr.getTax());
			System.out.println();
			
			System.out.println("-----------Nightly Rates----------- \n");			
			Map<String, String[]> nightlyRateMap = srr.getNightlyRate();
			for (Map.Entry<String, String[]> nRate : nightlyRateMap.entrySet()) {
				System.out.println("Date \t: " + nRate.getKey());
				String[] data = nRate.getValue();
				System.out.println("Price \t: " + data[0]);
				System.out.println("Tax \t: " + data[1]);
				System.out.println("Fee \t: " + data[2]);
				System.out.println("Price with Tax and Fee \t:" + data[3]);
				System.out.println();
			}
			System.out.println("--------- End of Night Rates Details ------- \n");
			
			System.out.println("--------Discount Nightly Rates-------- \n");			
			Map<String, String[]> dnightlyRateMap = srr.getDiscountNightlyRate();
			for (Map.Entry<String, String[]> dnRate : dnightlyRateMap.entrySet()) {
				System.out.println("Date \t: " + dnRate.getKey());
				String[] data = dnRate.getValue();
				System.out.println("Price \t: " + data[0]);
				System.out.println("Tax \t: " + data[1]);
				System.out.println("Fee \t: " + data[2]);
				System.out.println("Price with Tax and Fee \t" + data[3]);
				System.out.println();
			}
			System.out.println("--------- End of Dscount Night Rates Details ------- \n");
			
			System.out.println("Adult Count : \t" + srr.getAdultCount());
			System.out.println("Child Count : \t" + srr.getChildCount());
			System.out.println("Checkin : \t" + srr.getCheckin());
			System.out.println("Checkout : \t" + srr.getCheckout());
			System.out.println("Duration : \t" + srr.getDuration());
			System.out.println("Hotel Code : \t" + srr.getHotelCode());
			System.out.println("Hotel Name : \t" + srr.getHotelName());
			System.out.println("Adult Count : \t" + srr.getAdultCount());
			
			System.out.println("\n--------- Occupant Details ------- \n");
			
			ArrayList<Occupancy> occ = srr.getOccupant();
			for(Occupancy oc : occ){
				System.out.println("Name Prefix : \t" + oc.getNamePrefix());
				System.out.println("First Name : \t" + oc.getFirstName());
				System.out.println("Last Name : \t" + oc.getLastName());
				System.out.println("Telephone : \t" + oc.getTelephone());
				System.out.println("Email : \t" + oc.getEmail());
				System.out.println("Address Line : \t" + oc.getAddressLine());
				System.out.println("City Name : \t" + oc.getCity());
				System.out.println("Postal Code : \t" + oc.getPostalCode());
				System.out.println("State Code : \t" + oc.getStateCode());
				System.out.println("Country Code : \t" + oc.getCountryCode());
			}
			
			System.out.println("Card Code : \t" + srr.getCardcode());
			System.out.println("Card Number : \t" + srr.getCardNumber());
			System.out.println("Expire Date : \t" + srr.getExpireDate());
			System.out.println("Card Holder Name : \t" + srr.getCardHolderName());
			
			System.out.println("\n Hotel Reservation IDs \n");
			ArrayList<String> resID_Value = srr.getHotelReservationID();
			for(String ridv : resID_Value){
				System.out.println("ResID_Value : \t" + ridv);
			}
			
	 }
	 
		@After
		public void afterTest(){			
			System.out.println("Test is Done");
		}
}
