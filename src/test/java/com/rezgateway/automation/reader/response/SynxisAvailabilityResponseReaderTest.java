package com.rezgateway.automation.reader.response;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.SynxisAvailabilityResponse;
import com.rezgateway.automation.pojo.SynxisCancellationPolicy;
import com.rezgateway.automation.pojo.SynxisRoom;
import com.rezgateway.automation.reader.response.SynxisAvailabilityResponseReader;
import com.rezgateway.automation.utills.DocumentBuilder;

public class SynxisAvailabilityResponseReaderTest {
	
	static Logger logger;

	@Before
	public void setup(){
		System.out.println("Test Started");
	}
	
	public SynxisAvailabilityResponseReaderTest(){
		logger = Logger.getLogger(SynxisAvailabilityResponseReaderTest.class);
	}
	
	  @Test
	  public void readResponseData() throws JDOMException, IOException {

		  String filepath = "resources/synxisAvailWithChildResponse.xml";
			
			DocumentBuilder builder = new DocumentBuilder();
			Document doc = builder.docBuilder(filepath);
			
			SynxisAvailabilityResponseReader reader = new SynxisAvailabilityResponseReader();
			SynxisAvailabilityResponse response = new SynxisAvailabilityResponse();
			
			try {
				response = reader.responseReader(doc);
			} catch (Exception e) {
				logger.fatal("Data not read properly" + e);
			}
			
			System.out.println("Echo token \t:" + response.getEchoToken());
			System.out.println("Hotel code \t:" + response.getHotelCode());
			System.out.println("Hotel name \t:" + response.getHotelName() + " \n");
			System.out.println("=========== Room type details ==========");
			
			Map<String, String[]> roomTypeMap = response.getRoomTypes();
			for (Map.Entry<String, String[]> entry : roomTypeMap.entrySet()) {
				System.out.println("RoomType code \t: " +entry.getKey());
				String[] data = entry.getValue();
				System.out.println("No of units \t: " + data[0]);
				System.out.println("Room name \t: " + data[1]);
				System.out.println("Room desc \t: " + data[2] + "\n");
			}
			System.out.println("=========== End of room type details ========== \n");
			System.out.println("=========== RatePlan details ==========");
			
			Map<String, String[]> ratePlanMap = response.getRatePlans();
			for (Map.Entry<String, String[]> entry : ratePlanMap.entrySet()) {
				System.out.println("Rateplan code \t: " +entry.getKey());
				String[] rateData = entry.getValue();
				System.out.println("Rateplan name \t: " + rateData[0]);
				System.out.println("Rate desc \t: " + rateData[1] + "\n");
			}
			System.out.println("=========== End of rate plan details ========== \n");
			
			for(SynxisRoom room : response.getRoomList()){					
				for(SynxisCancellationPolicy policy : room.getCp()){
					System.out.println("Room type  \t:" + room.getRoomTypeCode());
					System.out.println("Rate plan  \t:" + room.getRatePlanCode());
					System.out.println("Total rate \t:" + room.getTotalAfterTax());
					System.out.println("Policy code \t:" + policy.getPolicyCode());
					System.out.println("Policy desc \t:" + policy.getPenltyDesc());
					System.out.println("Policy amount \t:" + policy.getAmount());
					System.out.println("Tax inclusive \t:" + policy.getIsTaxInclusive());	
					System.out.println("Deadline \t: " + policy.getDeadLine());
					System.out.println("OffsetTimeUnit \t: " + policy.getOffsetTimeUnit());
					System.out.println("OffsetUnitMultiplier \t: " + policy.getOffsetUnitMultiplier());
					System.out.println("OffsetDropTime \t: " + policy.getOffsetDropTime());
					System.out.println("========================================== \n");
				}				
			}		  
	  }
	  
		@After
		public void afterTest(){			
			System.out.println("Test is Done");
		}
}
