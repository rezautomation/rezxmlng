package com.rezgateway.automation.reader.response;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.rezgateway.automation.pojo.CancellationResponse;

public class CancellationResponseReaderTest {

	@Test
	public void testForNormalResponse() throws Exception {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new File("resources/SampleCancellationResponse.xml"));
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StringWriter strWriter = new StringWriter();
		StreamResult result = new StreamResult(strWriter);
		transformer.transform(source, result);
		String outString = strWriter.getBuffer().toString();

		CancellationResponseReader canRes = new CancellationResponseReader();
		CancellationResponse readResponsObj = canRes.getResponse(outString);
		System.out.println(readResponsObj.getCancellationNo().trim());
		canRes.buildResponse("resources/Sample_Build_CNX_Response.xml", outString);

	}

	@Test
	public void testWithErrorResponse() throws SAXException, IOException, Exception {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(new File("resources/SampleCancellationResponseWithErrors.xml"));
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		StringWriter strWriter = new StringWriter();
		StreamResult result = new StreamResult(strWriter);
		transformer.transform(source, result);
		String outString = strWriter.getBuffer().toString();

		CancellationResponseReader canRes = new CancellationResponseReader();
		CancellationResponse readResponsObj = canRes.getResponse(outString);
		System.out.println(readResponsObj.getErrorDescription().trim());

		canRes.buildResponse("resources/Sample_Build_CNX_Response.xml", outString);
		
	}

}
