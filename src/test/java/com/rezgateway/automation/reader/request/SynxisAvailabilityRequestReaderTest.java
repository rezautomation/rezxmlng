package com.rezgateway.automation.reader.request;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.SynxisAvailabilityRequest;
import com.rezgateway.automation.reader.request.SynxisAvailabilityRequestReader;
import com.rezgateway.automation.utills.DocumentBuilder;

public class SynxisAvailabilityRequestReaderTest {
	
	static Logger logger;

	@Before
	public void setup(){
		System.out.println("Test Started");
	}
	
	public SynxisAvailabilityRequestReaderTest(){
		logger = Logger.getLogger(SynxisAvailabilityRequestReaderTest.class);
	}
	
  @Test
  public void readReqData() throws JDOMException, IOException {

		String filepath = "resources/synxisAvailWithChildRequest.xml";
		
		DocumentBuilder builder = new DocumentBuilder();
		Document doc = builder.docBuilder(filepath);
		
		SynxisAvailabilityRequestReader reader = new SynxisAvailabilityRequestReader();
		SynxisAvailabilityRequest req = new SynxisAvailabilityRequest();
		
		try {
			req = reader.RequestReader(doc);
		} catch (Exception e) {
			logger.fatal("Document not read properly : " + e);
		}
		System.out.println("Username \t: " + req.getUsername());
		System.out.println("Password \t: " + req.getPassword());
		System.out.println("Echo token \t: " + req.getEchoToken());
		System.out.println("CheckIn \t: " + req.getCheckin());
		System.out.println("CheckOut \t: " + req.getCheckout());
		System.out.println("rate plan \t: " + req.getRateplan());
		System.out.println("Room quantity \t: " + req.getRoomCount());
		System.out.println("Adult count \t: " + req.getAdultCount());
		System.out.println("Child count \t: " + req.getChildCount());	
	  
  }
  
	@After
	public void afterTest(){			
		System.out.println("Test is Done");
	}
}
