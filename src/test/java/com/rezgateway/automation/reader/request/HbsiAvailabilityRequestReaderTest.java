package com.rezgateway.automation.reader.request;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.HbsiAvailabilityRequest;
import com.rezgateway.automation.utills.DocumentBuilder;

public class HbsiAvailabilityRequestReaderTest {
	static Logger logger;

	@Before
	public void setup(){
		System.out.println("Test Started");
	}
	
	public HbsiAvailabilityRequestReaderTest(){
		logger = Logger.getLogger(HbsiAvailabilityRequestReaderTest.class);
	}
	
  @Test
  public void readReqData() throws JDOMException, IOException {
	  String filepath = "resources/HbsiAvailWithChildRequest.xml";
		
		DocumentBuilder builder = new DocumentBuilder();
		Document doc = builder.docBuilder(filepath);
		
		HbsiAvailabilityRequestReader reader = new HbsiAvailabilityRequestReader();
		HbsiAvailabilityRequest req = new HbsiAvailabilityRequest();
		reader.RequestReader(doc);
		try {
			req = reader.RequestReader(doc);
		} catch (Exception e) {
			logger.fatal("Document not read properly : " + e);
		}
		
		System.out.println("CheckIn \t: " + req.getCheckin());
		System.out.println("CheckOut \t: " + req.getCheckout());
		
		System.out.println("=========== RatePlan details ==========");
		Map<String, String[]> ratePlanCandidateMap = req.getRatePlanCandidate();
		for (Map.Entry<String, String[]> entry : ratePlanCandidateMap.entrySet()) {
			System.out.println("RPH value \t: " +entry.getKey());
			String[] data = entry.getValue();
			System.out.println("Hotel code \t: " + data[0]);
			System.out.println("Rate plan code \t: " + data[1]);
			System.out.println("Meal plan code \t: " + data[2] + "\n");
		}
		System.out.println("=========== End of RatePlan details ========== \n");
		
		System.out.println("=========== Room stay details ==========");
		Map<String, String[]> roomStayMap = req.getRoomStayCandidate();
		for (Map.Entry<String, String[]> entry : roomStayMap.entrySet()) {
			System.out.println("RPH value \t: " +entry.getKey());
			String[] data = entry.getValue();
			System.out.println("Room type code \t: " + data[0]);
			System.out.println("Quantity \t: " + data[1]);
			System.out.println("Adult count \t: " + data[2]);
			System.out.println("Child count \t: " + data[3] + "\n");
		}
		System.out.println("=========== End of RoomStay details ==========");
  }
  
  @After
	public void afterTest(){			
		System.out.println("Test is Done");
	}

}
