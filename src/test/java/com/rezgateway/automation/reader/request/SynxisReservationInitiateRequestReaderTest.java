/**
 * 
 */
package com.rezgateway.automation.reader.request;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.Occupancy;
import com.rezgateway.automation.pojo.SynxisReservationInitiateRequest;
import com.rezgateway.automation.utills.DocumentBuilder;

/**
 * @author Dinethra
 *
 */

public class SynxisReservationInitiateRequestReaderTest {
	
	static Logger logger;

	@Before
	public void setup(){
		System.out.println("Test Started");
	}
	
	public SynxisReservationInitiateRequestReaderTest(){
		logger = Logger.getLogger(SynxisReservationInitiateRequestReaderTest.class);
	}
	
	@Test
	public void synxisReservationInitiateRequestReadData() throws JDOMException, IOException{
		
		String filepath = "resources/SynxisResInitiateRequest.xml";
		DocumentBuilder builder = new DocumentBuilder();
		Document doc = builder.docBuilder(filepath);
		
		SynxisReservationInitiateRequestReader srirReader = new SynxisReservationInitiateRequestReader();
		SynxisReservationInitiateRequest srir = new SynxisReservationInitiateRequest();
		
		try {
			srir = srirReader.RequestReader(doc);
		} catch (Exception e) {
			logger.fatal("Document not read properly : " + e);
		}
		
		System.out.println("Username \t: " + srir.getUsername());
		System.out.println("Password \t: " + srir.getPassword());
		System.out.println("Room Code \t: " + srir.getRoomCode());
		System.out.println("Room Count \t: " + srir.getRoomCount());
		System.out.println("Rate Plan Code \t: " + srir.getRatePlanCode());
		System.out.println("Adult Count \t: " + srir.getAdultCount());
		System.out.println("Child Count \t: " + srir.getChildCount());
		System.out.println("Checkin \t: " + srir.getCheckin());
		System.out.println("Checkout \t: " + srir.getCheckout());
		System.out.println("Hotel Code \t: " + srir.getHotelCode());
		System.out.println("Checkout \t: " + srir.getCheckout());
		
		ArrayList<Occupancy> occ = srir.getOccupant();
		for(Occupancy oc : occ){
			System.out.println("Name Prefix \t: " + oc.getNamePrefix());
			System.out.println("Given Name \t: " + oc.getFirstName());
			System.out.println("Suraname \t: " + oc.getLastName());
			System.out.println("Telephone \t: " + oc.getTelephone());
			System.out.println("Email \t: " + oc.getEmail());
			System.out.println("Address Line \t: " + oc.getAddressLine());
			System.out.println("City Name \t: " + oc.getCity());
			System.out.println("Postal Code \t: " + oc.getPostalCode());
			System.out.println("State Code \t: " + oc.getStateCode());
			System.out.println("Country Code \t: " + oc.getCountryCode());
		}
		
		System.out.println("Card Code \t: " + srir.getCardcode());
		System.out.println("Card Number \t: " + srir.getCardNumber());
		System.out.println("Series Code \t: " + srir.getSeriesCode());
		System.out.println("Expire Date \t: " + srir.getExpireDate());
		System.out.println("Card Holder Name \t: " + srir.getCardHolderName());
	}
	
	@After
	public void afterTest(){			
		System.out.println("Test is Done");
	}
}
