package com.rezgateway.automation.reader.request;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.pojo.HbsiReservationRequest;
import com.rezgateway.automation.pojo.HbsiRoom;
import com.rezgateway.automation.pojo.Occupancy;
import com.rezgateway.automation.utills.DocumentBuilder;

public class HbsiReservationRequestReaderTest {
	static Logger logger;

	@Before
	public void setup(){
		System.out.println("Test Started");
	}
	
	public HbsiReservationRequestReaderTest(){
		logger = Logger.getLogger(HbsiReservationRequestReaderTest.class);
	}
	
  @Test
  public void readReqData() throws JDOMException, IOException{
	  String filepath = "resources/HbsiReservationRequet.xml";
	  
	  DocumentBuilder builder = new DocumentBuilder();
	  Document doc = builder.docBuilder(filepath);
		
	  HbsiReservationRequestReader reader = new HbsiReservationRequestReader();
	  HbsiReservationRequest req = new HbsiReservationRequest();
	  
	  try {
		  req = reader.RequestReader(doc);
	  } catch (Exception e) {
		  logger.fatal("Document not read properly : " + e);
	  }
	  System.out.println("uniqueID \t\t: " + req.getUniqueId());
	  System.out.println("Check-In \t\t: " + req.getCheckin());
	  System.out.println("Check-Out \t\t: " + req.getCheckout() + "\n");
	  
	  for(HbsiRoom room : req.getRoomList()){
			System.out.println("Hotel code \t\t: " + room.getHotelCode());			
			System.out.println("Room type  \t\t: " + room.getRoomTypeCode());
			System.out.println("Rate plan  \t\t: " + room.getRatePlanCode());
			System.out.println("Total before tax \t: " + room.getTotalBeforeTax());
			System.out.println("Total after tax \t: " + room.getTotalAfterTax());
			System.out.println("Adult count \t\t: " + room.getAdultCount());
			System.out.println("Child count \t\t: " + room.getChildCount() + "\n");
		}
	  
	  System.out.println("Card no \t\t: " + req.getCardNo());
	  System.out.println("Card code \t\t: " + req.getCardCode());
	  System.out.println("Card type \t\t: " + req.getCardType());
	  System.out.println("Exp date \t\t: " + req.getExpireDate());
	  System.out.println("Card series \t\t: " + req.getSeriesCode());
	  System.out.println("Card holder name \t: " + req.getCardHolderName() + "\n");
	  
	  for (Occupancy occupant : req.getOccupant()){
		  System.out.println("Title \t\t\t: " + occupant.getNamePrefix());
		  System.out.println("First name \t\t: " + occupant.getFirstName());
		  System.out.println("Last name \t\t: " + occupant.getLastName());
		  System.out.println("Phone no \t\t: " + occupant.getTelephone());
		  System.out.println("Address \t\t: " + occupant.getAddressLine() + ", " + occupant.getCity() + ", " +
				  occupant.getPostalCode() + ", " + occupant.getStateCode() + ", " + occupant.getCountryCode());
				  
	  }  
  }  

}
