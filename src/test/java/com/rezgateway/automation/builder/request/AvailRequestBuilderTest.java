package com.rezgateway.automation.builder.request;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.rezgateway.automation.builder.request.AvailabilityRequestBuilder;
import com.rezgateway.automation.pojo.Room;
import com.rezgateway.automation.pojo.AvailabilityRequest;
//import com.rezgateway.automation.reader.response.AvailabilityResponseReader;

/**
 * Unit test for AvailRequestBuilderTest.
 */
public class AvailRequestBuilderTest {

	AvailabilityRequestBuilder builderObj = new AvailabilityRequestBuilder();
	Room r1 = new Room();
	Room r2 = new Room();
	AvailabilityRequest requestObj = new AvailabilityRequest();

	@Before
	public void Setup() {
		requestObj.setAvailabitlyCancerlatioPolicy("Y");
		requestObj.setAvailabitlyHotelFee("y");
		requestObj.setUserName("TesthotelName");
		requestObj.setPassword("Akila");
		requestObj.setCheckin("24-MAR-2018");
		requestObj.setCheckout("28-MAR-2018");
		requestObj.setNoofNights("4");
		requestObj.setCountry("US");
		requestObj.setState("NY");
		requestObj.setSearchType("cityCode");
		String[] hotelCode =new String[1];
		hotelCode[0]="CY191";
		requestObj.setCode(hotelCode);
		/*String[] hotelCode =new String[10];
		if (hotelCode.length==0) {
			System.out.println("hotel code Empty");
		} else {
			for (int i = 0; i < hotelCode.length; i++) {
				hotelCode[i] = Integer.toString(i);
				requestObj.setCode(hotelCode);
			}
		}*/
		requestObj.setCode(hotelCode);
		r1.setRoomTypeID("STD");
		r1.setBedTypeID("DUB");
		r1.setAdultsCount("2");
		r1.setChildCount("0");
		//String[] childAges1 = new String[1];
		//r1.setChildAges(childAges1);
		r2.setRoomTypeID("DLX");
		r2.setBedTypeID("TPL");
		r2.setAdultsCount("1");
		r2.setChildCount("2");
		String[] childAges2 = new String[2];
		childAges2[0] = "3";
		childAges2[1] = "4";
		r2.setChildAges(childAges2);
		ArrayList<Room> roomList = new ArrayList<Room>();
		roomList.add(r1);
		roomList.add(r2);
		requestObj.setRoomlist(roomList);
	}

	@Test
	public void TestSuite() throws IOException {

		String URL = "resources/Sample_AvailRequest2.xml";
		String out = null;
		try {
			out = builderObj.buildRequest(URL, requestObj);
			/*AvailabilityResponseReader res = new AvailabilityResponseReader();
			res.getResponse(out);*/
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(out);
	}

	@After
	public void tearDown() {

		try {
			System.out.println("Test is Done");
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
