package com.rezgateway.automation.builder.request;

import java.io.FileWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom2.Element;
import org.jdom2.Document;
import org.jdom2.Attribute;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.rezgateway.automation.enu.ConfirmationType;
import com.rezgateway.automation.enu.ModificationType;
import com.rezgateway.automation.pojo.ModificationRequest;
import com.rezgateway.automation.pojo.Room;

public class ModificationRequestBuilder {

	private static Logger logger = null;

	public ModificationRequestBuilder() {
		logger = Logger.getLogger(ModificationRequestBuilder.class);
	}

	public Document getModifyTypeOption(ModificationRequest reqObj) throws Exception {

		if (ModificationType.GUESTDETAILS == reqObj.getModifyType()) {
			return getDocumentForModifyReservation(reqObj);
		} else if (ModificationType.AMENDNOTE == reqObj.getModifyType()) {
			return getDocumentForModifyNote(reqObj);
		} else if (ModificationType.STAYPERIOD == reqObj.getModifyType()) {
			return getDocumentForModifyReservation(reqObj);
		} else if (ModificationType.AMENDROOM == reqObj.getModifyType()) {
			return getDocumentForModifyReservation(reqObj);
		} else if (ModificationType.ADDROOM == reqObj.getModifyType()) {
			return getDocumentForModifyReservation(reqObj);
		} else if (ModificationType.TOUROPERAORNUM == reqObj.getModifyType()) {
			return getDocumentForModifyTO(reqObj);
		} else {
			logger.error("User Modify Selection is Wrong Please Enter relevant Modification Type " + reqObj.getScenarioID());
			throw new Exception("User Modify Selection is Wrong Please Enter relevant Modification Type ");
		}

	}

	public Document getDocumentForModifyReservation(ModificationRequest reqObj) {

		Document doc = new Document();
		Element modifyReservationRequest = new Element("modifyReservationRequest");

		try {
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Starting.........." + reqObj.getScenarioID());
			doc.setRootElement(modifyReservationRequest);
			Element controls = new Element("control");
			controls.addContent(new Element("username").setText(reqObj.getUserName()));
			controls.addContent(new Element("password").setText(reqObj.getPassword()));
			doc.getRootElement().addContent(controls);
			Element modifyReservationDetails = new Element("modifyReservationDetails");
			modifyReservationDetails.setAttribute("timeStamp", reqObj.getModificationDetailsTimeStamp());
			
			if(reqObj.getModifyType().toString() == "GUESTDETAILS")
				modifyReservationDetails.setAttribute("modifyType", "guestDetails");
			else if(reqObj.getModifyType().toString() == "STAYPERIOD")
				modifyReservationDetails.setAttribute("modifyType", "stayPeriod");
			else if(reqObj.getModifyType().toString() == "AMENDROOM")
				modifyReservationDetails.setAttribute("modifyType", "amendRoom");
			else if(reqObj.getModifyType().toString() == "ADDROOM")
				modifyReservationDetails.setAttribute("modifyType", "addRoom");
			
			modifyReservationDetails.addContent(new Element("referenceNo").setText(reqObj.getReferenceno()));
			
			if (ConfirmationType.CON == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("CON"));
			} else if (ConfirmationType.REQ == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("REQ"));
			} else {
				logger.error("........ERROR_ConfType_Selection.........." + reqObj.getScenarioID());
				modifyReservationDetails.addContent(new Element("confirmationType").setText("ERROR_ConfType_Selection"));
			}
			modifyReservationDetails.addContent(new Element("tourOperatorOrderNumber").setText(reqObj.getTourOperatorOrderNumber()));
			modifyReservationDetails.addContent(new Element("checkIn").setText(reqObj.getModCheckin()));
			modifyReservationDetails.addContent(new Element("CheckOut").setText(reqObj.getModCheckout()));
			modifyReservationDetails.addContent(new Element("noOfRooms").setText(reqObj.getNoOfRooms()));
			modifyReservationDetails.addContent(new Element("noOfNights").setText(reqObj.getNoofNights()));
			modifyReservationDetails.addContent(new Element("hotelCode").setText(reqObj.getCode()[0]));
			modifyReservationDetails.addContent(new Element("total").setAttribute("currency", reqObj.getCurrency()).setText(reqObj.getModTotal()));
			modifyReservationDetails.addContent(new Element("totalTax").setAttribute("currency", reqObj.getCurrency()).setText(reqObj.getModTotalTax()));

			// //
			if ((reqObj.getRezRoomList().size() > 0) && (reqObj.getRezRoomList() != null)) {

				for (Room r : reqObj.getRezRoomList()) {

					Element roomData = new Element("roomData");
					modifyReservationDetails.addContent(roomData);
					roomData.addContent(new Element("roomNo").setText(r.getRoomNo()));
					roomData.addContent(new Element("roomResNo").setText(r.getRoomReferenceNo()));
					roomData.addContent(new Element("roomCode").setText(r.getRoomCode()));
					roomData.addContent(new Element("roomTypeCode").setText(r.getRoomTypeID()));
					roomData.addContent(new Element("bedTypeCode").setText(r.getBedTypeID()));
					roomData.addContent(new Element("ratePlanCode").setText(r.getRatePlanCode()));
					roomData.addContent(new Element("noOfAdults").setText(r.getAdultsCount()));
					roomData.addContent(new Element("noOfChildren").setText(r.getChildCount()));

					// //Note >>>>>> Last name and First name Counts Should be Equal to adults+Child Count
					int paxCount = (Integer.parseInt((r.getAdultsCount()))) + (Integer.parseInt((r.getChildCount())));
					Element occupancy = new Element("occupancy");

					if (paxCount != 0) {
						roomData.addContent(occupancy);

						for (int i = 0; i < (r.getAdultsOccpancy().length); i++) {

							Element guest = new Element("guest");
							String[] adultnamesList = r.getAdultsOccpancy();
							String[] fullName = adultnamesList[i].split("_");
							guest.addContent(new Element("title").setText(fullName[0]));
							guest.addContent(new Element("firstName").setText(fullName[1]));
							guest.addContent(new Element("lastname").setText(" Adult_" + (i + 1) + "_Lastname"));
							occupancy.addContent(guest);

						}
						if (r.getChildCount() != null) {
							if (!("0").equals(r.getChildCount())) {
								for (int i = 0; i < (r.getChildOccupancy().length); i++) {

									String[] childnameList = r.getChildOccupancy();
									String[] childAgesList = r.getChildAges();
									Element guest = new Element("guest");
									String[] fullName = childnameList[i].split("_");
									guest.addContent(new Element("title").setText(fullName[0]));
									guest.addContent(new Element("firstName").setText(fullName[1]));
									guest.addContent(new Element("lastname").setText(" Child_" + (i + 1) + "_Lastname"));
									guest.addContent(new Element("age").setText(childAgesList[i]));
									occupancy.addContent(guest);

								}
							} else {
								//this will happen , when Child Count is Zero 
							}
						} else {
							logger.fatal("........Child Count is Null.........." + reqObj.getScenarioID());
						}
					} else {
						logger.error("........Error PaxCount entered.........." + reqObj.getScenarioID());
						roomData.addContent(occupancy).setText("Error PaxCount entered");
						doc.getRootElement().addContent(modifyReservationDetails);
						// throw new Exception(" name Count and PAX count is not matched ");
					}

				}

			} else {
				logger.error("........No Rooms Availble for This hotel or No Rooom Data Provided.........."+ reqObj.getScenarioID());
				modifyReservationDetails.addContent("error").setText("No Rooms Availble for This hotel or No Rooom Data Provided");
				doc.getRootElement().addContent(modifyReservationDetails);
			}

			Element comment = new Element("comment");
			modifyReservationDetails.addContent(comment);
			comment.addContent(new Element("Customer").setText(reqObj.getUserMODComment()));
			comment.addContent(new Element("Hotel").setText(reqObj.getHotelMODComment()));
			doc.getRootElement().addContent(modifyReservationDetails);
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Ending.........."+ reqObj.getScenarioID());

		} catch (Exception e) {
			logger.fatal("Error While Building Doc :"+ reqObj.getScenarioID(), e);
		}

		return doc;

	}

	public Document getDocumentForModifyNote(ModificationRequest reqObj) {

		Document doc = new Document();
		Element modifyReservationRequest = new Element("modifyReservationRequest");

		try {
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Starting.........."+ reqObj.getScenarioID());
			modifyReservationRequest.setAttribute(new Attribute("Version", reqObj.getVersion()));
			doc.setRootElement(modifyReservationRequest);
			Element controls = new Element("control");
			controls.addContent(new Element("username").setText(reqObj.getUserName()));
			controls.addContent(new Element("password").setText(reqObj.getPassword()));
			doc.getRootElement().addContent(controls);
			Element modifyReservationDetails = new Element("modifyReservationDetails");
			modifyReservationDetails.setAttribute("timeStamp", reqObj.getModificationDetailsTimeStamp());
			modifyReservationDetails.setAttribute("modifyType", "amendNotes");
			modifyReservationDetails.addContent(new Element("ReferenceNo").setText(reqObj.getReferenceno()));

			if (ConfirmationType.CON == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("CON"));
			} else if (ConfirmationType.REQ == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("REQ"));
			} else {
				logger.error("........ERROR_ConfType_Selection.........."+ reqObj.getScenarioID());
				modifyReservationDetails.addContent(new Element("confirmationType").setText("ERROR_ConfType_Selection"));
			}
			modifyReservationDetails.addContent(new Element("tourOperatorOrderNumber").setText(reqObj.getTourOperatorOrderNumber()));
			Element comment = new Element("comment");
			modifyReservationDetails.addContent(comment);
			comment.addContent(new Element("Customer").setText(reqObj.getUserMODComment()));
			comment.addContent(new Element("Hotel").setText(reqObj.getHotelMODComment()));
			modifyReservationRequest.addContent(modifyReservationDetails);
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Ending.........."+ reqObj.getScenarioID());
		} catch (Exception e) {
			logger.fatal("Error While Building Document :"+ reqObj.getScenarioID(), e);
		}

		return doc;

	}

	public Document getDocumentForModifyTO(ModificationRequest reqObj) {

		Document doc = new Document();
		Element modifyReservationRequest = new Element("modifyReservationRequest");

		try {
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Starting.........."+ reqObj.getScenarioID());
			modifyReservationRequest.setAttribute(new Attribute("Version", reqObj.getVersion()));
			doc.setRootElement(modifyReservationRequest);
			Element controls = new Element("control");
			controls.addContent(new Element("username").setText(reqObj.getUserName()));
			controls.addContent(new Element("password").setText(reqObj.getPassword()));
			doc.getRootElement().addContent(controls);
			Element modifyReservationDetails = new Element("modifyReservationDetails");
			modifyReservationDetails.setAttribute("timeStamp", reqObj.getModificationDetailsTimeStamp());
			modifyReservationDetails.setAttribute("modifyType", "tourOpOrderNum");
			modifyReservationDetails.addContent(new Element("ReferenceNo").setText(reqObj.getReferenceno()));

			if (ConfirmationType.CON == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("CON"));
			} else if (ConfirmationType.REQ == reqObj.getConfType()) {
				modifyReservationDetails.addContent(new Element("confirmationType").setText("REQ"));
			} else {
				logger.error("........ERROR_ConfType_Selection.........."+ reqObj.getScenarioID());
				modifyReservationDetails.addContent(new Element("confirmationType").setText("ERROR_ConfType_Selection"));
			}
			modifyReservationDetails.addContent(new Element("oldTourOperatorOrderNumber").setText(reqObj.getTourOperatorOrderNumber()));
			modifyReservationDetails.addContent(new Element("newTourOperatorOrderNumber").setText(reqObj.getNewToOrderNo()));
			
			Element comment = new Element("comment");
			modifyReservationDetails.addContent(comment);
			comment.addContent(new Element("Customer").setText(reqObj.getUserMODComment()));
			comment.addContent(new Element("Hotel").setText(reqObj.getHotelMODComment()));
			modifyReservationRequest.addContent(modifyReservationDetails);
			logger.info("........" + reqObj.getModifyType().toString() + "_modifyReservationRequest__Document Ending.........."+ reqObj.getScenarioID());
		} catch (Exception e) {
			logger.fatal("Error While Building Document :"+ reqObj.getScenarioID(), e);
		}

		return doc;

	}
	
	public String buildRequest(String Path, ModificationRequest modRequest) throws Exception {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getModifyTypeOption(modRequest);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, new FileWriter(Path));
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Document :"+ modRequest.getScenarioID(), es);
		}
		return res;
	}

	public String buildRequest(ModificationRequest resObj) {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getModifyTypeOption(resObj);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Document :"+ resObj.getScenarioID(), es);
		}
		return res;

	}

}
