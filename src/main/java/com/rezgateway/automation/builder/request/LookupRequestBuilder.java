package com.rezgateway.automation.builder.request;

import java.io.FileWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import com.rezgateway.automation.pojo.LookupRequest;

public class LookupRequestBuilder {

	private static Logger logger = null;
	
	public LookupRequestBuilder(){
		logger = Logger.getLogger(LookupRequestBuilder.class);
	}
	
	public Document getDocument(LookupRequest reqObj){
		
		Document doc = new Document();
		Element lookupReservationRequest = new Element("lookupReservationRequest");
		
		try{
			logger.info("........_ReservationRequest_Document Starting..........");
			lookupReservationRequest.setAttribute(new Attribute("timeStamp", reqObj.getTimestamp()));
			doc.setRootElement(lookupReservationRequest);
			
			Element controls = new Element("control");
			controls.addContent(new Element("username").setText(reqObj.getUserName()));
			controls.addContent(new Element("password").setText(reqObj.getPassword()));
			doc.getRootElement().addContent(controls);
			
			new Element("referenceNo").setText(reqObj.getReferenceno());
			logger.info("........_ReservationRequest_Document Ending..........");
			
		}catch(Exception e){
			logger.fatal("Error While Building Document. ", e);
		}
		return doc;
	}
	
	public String buildRequest(String Path, LookupRequest resObj) throws Exception {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getDocument(resObj);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, new FileWriter(Path));
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Document. ", es);
		}
		return res;
	}

	public String buildRequest(LookupRequest resObj) {

		StringWriter sw = new StringWriter();
		String res = null;
		Document xmlDoc = null;
		try {
			xmlDoc = getDocument(resObj);
			XMLOutputter xmlOutput = new XMLOutputter();
			xmlOutput.setFormat(Format.getPrettyFormat());
			xmlOutput.output(xmlDoc, sw);
			res = sw.getBuffer().toString();
		} catch (Exception es) {
			logger.fatal("Error While Building Document. ", es);
		}
		return res;
	}
}
