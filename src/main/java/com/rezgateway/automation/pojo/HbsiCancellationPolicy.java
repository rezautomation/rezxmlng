package com.rezgateway.automation.pojo;

public class HbsiCancellationPolicy {
	private String policyCode			= null;
	private String nonRefundable		= null;
	private String deadLine				= null;	
	private String penltyDesc			= null;
	private String amountPercentUnit	= null;
	private String amountPercentAmnt	= null;
	
	public String getPolicyCode() {
		return policyCode;
	}
	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}
	public String getNonRefundable() {
		return nonRefundable;
	}
	public void setNonRefundable(String nonRefundable) {
		this.nonRefundable = nonRefundable;
	}
	public String getDeadLine() {
		return deadLine;
	}
	public void setDeadLine(String deadLine) {
		this.deadLine = deadLine;
	}
	public String getPenltyDesc() {
		return penltyDesc;
	}
	public void setPenltyDesc(String penltyDesc) {
		this.penltyDesc = penltyDesc;
	}
	public String getAmountPercentUnit() {
		return amountPercentUnit;
	}
	public void setAmountPercentUnit(String amountPercentUnit) {
		this.amountPercentUnit = amountPercentUnit;
	}
	public String getAmountPercentAmnt() {
		return amountPercentAmnt;
	}
	public void setAmountPercentAmnt(String amountPercentAmnt) {
		this.amountPercentAmnt = amountPercentAmnt;
	}
	
}
