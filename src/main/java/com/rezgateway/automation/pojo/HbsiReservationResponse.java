package com.rezgateway.automation.pojo;

import java.util.HashMap;
import java.util.Map;

public class HbsiReservationResponse {
	private Map<String, String[]> resDetails	= new HashMap<String, String[]>();

	public Map<String, String[]> getResDetails() {
		return resDetails;
	}

	public void setResDetails(Map<String, String[]> resDetails) {
		this.resDetails = resDetails;
	}
	
}
