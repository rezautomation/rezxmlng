package com.rezgateway.automation.pojo;

public class ReservationResponse extends ReservationRequest {

	private String ReservationResponseStatus = null;
	private String Referenceno = null;
	private String RoomReferenceno = null;
	private String Touroperatorname = null;
	private String BookedHotelname = null;
	private String BookedHotelCode = null;
	private String Reservationsource = null;
	private String Reservationstatus = null;
	private String Reservationdate = null;
	private String Totalnoofadults = null;
	private String Totalnoofchildren = null;
	private String ErrorCode = null;
	private String ErrorDescription = null;
	
	
	/**
	 * @return the referenceno
	 */
	public String getReferenceno() {
		return Referenceno;
	}

	/**
	 * @return the roomReferenceno
	 */
	public String getRoomReferenceno() {
		return RoomReferenceno;
	}

	/**
	 * @return the touroperatorname
	 */
	public String getTouroperatorname() {
		return Touroperatorname;
	}

	/**
	 * @return the reservationsource
	 */
	public String getReservationsource() {
		return Reservationsource;
	}

	/**
	 * @return the reservationstatus
	 */
	public String getReservationstatus() {
		return Reservationstatus;
	}

	/**
	 * @return the reservationdate
	 */
	public String getReservationdate() {
		return Reservationdate;
	}

	/**
	 * @return the totalnoofadults
	 */
	public String getTotalnoofadults() {
		return Totalnoofadults;
	}

	/**
	 * @return the totalnoofchildren
	 */
	public String getTotalnoofchildren() {
		return Totalnoofchildren;
	}

	/**
	 * @param referenceno
	 *            the referenceno to set
	 */
	public void setReferenceno(String referenceno) {
		Referenceno = referenceno;
	}

	/**
	 * @param roomReferenceno
	 *            the roomReferenceno to set
	 */
	public void setRoomReferenceno(String roomReferenceno) {
		RoomReferenceno = roomReferenceno;
	}

	/**
	 * @param touroperatorname
	 *            the touroperatorname to set
	 */
	public void setTouroperatorname(String touroperatorname) {
		Touroperatorname = touroperatorname;
	}

	/**
	 * @param reservationsource
	 *            the reservationsource to set
	 */
	public void setReservationsource(String reservationsource) {
		Reservationsource = reservationsource;
	}

	/**
	 * @param reservationstatus
	 *            the reservationstatus to set
	 */
	public void setReservationstatus(String reservationstatus) {
		Reservationstatus = reservationstatus;
	}

	/**
	 * @param reservationdate
	 *            the reservationdate to set
	 */
	public void setReservationdate(String reservationdate) {
		Reservationdate = reservationdate;
	}

	/**
	 * @param totalnoofadults
	 *            the totalnoofadults to set
	 */
	public void setTotalnoofadults(String totalnoofadults) {
		Totalnoofadults = totalnoofadults;
	}

	/**
	 * @param totalnoofchildren
	 *            the totalnoofchildren to set
	 */
	public void setTotalnoofchildren(String totalnoofchildren) {
		Totalnoofchildren = totalnoofchildren;
	}

	/**
	 * @return the rreservationResponseStatus
	 */
	public String getRreservationResponseStatus() {
		return ReservationResponseStatus;
	}

	/**
	 * @param rreservationResponseStatus
	 *            the rreservationResponseStatus to set
	 */
	public void setRreservationResponseStatus(String rreservationResponseStatus) {
		ReservationResponseStatus = rreservationResponseStatus;
	}

	/**
	 * @return the bookedHotelname
	 */
	public String getBookedHotelname() {
		return BookedHotelname;
	}

	/**
	 * @param bookedHotelname
	 *            the bookedHotelname to set
	 */
	public void setBookedHotelname(String bookedHotelname) {
		BookedHotelname = bookedHotelname;
	}

	/**
	 * @return the bookedHotelCode
	 */
	public String getBookedHotelCode() {
		return BookedHotelCode;
	}

	/**
	 * @param bookedHotelCode
	 *            the bookedHotelCode to set
	 */
	public void setBookedHotelCode(String bookedHotelCode) {
		BookedHotelCode = bookedHotelCode;
	}

	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return ErrorCode;
	}

	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}

	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return ErrorDescription;
	}

	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		ErrorDescription = errorDescription;
	}

}
