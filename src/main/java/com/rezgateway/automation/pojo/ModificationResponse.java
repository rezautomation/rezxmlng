package com.rezgateway.automation.pojo;

public class ModificationResponse {
	
	private String                      ScenarioID									= null;
	private String 						ModificationResponseStatus					= null;
	private String 						ReferenceNo									= null;
	private String 						NewTotalAmount 								= null;
	private String						ModificationFee								= null;
	private String 						ErrorCode 									= null;
	private String 						ErrorDescription 							= null;
	/**
	 * @return the scenarioID
	 */
	public String getScenarioID() {
		return ScenarioID;
	}
	/**
	 * @return the modificationResponseStatus
	 */
	public String getModificationResponseStatus() {
		return ModificationResponseStatus;
	}
	/**
	 * @return the referenceNo
	 */
	public String getReferenceNo() {
		return ReferenceNo;
	}
	/**
	 * @return the newTotalAmount
	 */
	public String getNewTotalAmount() {
		return NewTotalAmount;
	}
	/**
	 * @return the modificationFee
	 */
	public String getModificationFee() {
		return ModificationFee;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return ErrorCode;
	}
	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return ErrorDescription;
	}
	/**
	 * @param scenarioID the scenarioID to set
	 */
	public void setScenarioID(String scenarioID) {
		ScenarioID = scenarioID;
	}
	/**
	 * @param modificationResponseStatus the modificationResponseStatus to set
	 */
	public void setModificationResponseStatus(String modificationResponseStatus) {
		ModificationResponseStatus = modificationResponseStatus;
	}
	/**
	 * @param referenceNo the referenceNo to set
	 */
	public void setReferenceNo(String referenceNo) {
		ReferenceNo = referenceNo;
	}
	/**
	 * @param newTotalAmount the newTotalAmount to set
	 */
	public void setNewTotalAmount(String newTotalAmount) {
		NewTotalAmount = newTotalAmount;
	}
	/**
	 * @param modificationFee the modificationFee to set
	 */
	public void setModificationFee(String modificationFee) {
		ModificationFee = modificationFee;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}
	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		ErrorDescription = errorDescription;
	}
	
	
	
}
