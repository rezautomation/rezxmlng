package com.rezgateway.automation.pojo;

import java.util.ArrayList;

import com.rezgateway.automation.enu.SearchBy;

public class AvailabilityRequest {

	private String 								ScenarioID 						= null;
	private String								AvailabitlyCancerlatioPolicy 	= "Y";
	private String 								AvailabitlyHotelFee 			= "Y";
	private String 								UserName 						= "TestDynamicRateTO";
	private String 								Password	 					= "ty5@test12345";
	private String 								Checkin 						= null;
	private String 								Checkout 						= null;
	private String 								NoofNights 						= null;
	private String 								NoOfRooms 						= null;
	private String 								Country 						= "US";
	private String 								State 							= "NY";
	private String[] 							Code 							= { "6109" };
	private ArrayList<Room> 					Roomlist						= new ArrayList<Room>();
	private SearchBy 							SearchType 						= SearchBy.NONE;
	private ArrayList<String>					ExpectedTotal					= null;
	private ArrayList<String>					ExpectedTax						= null;
	private ArrayList<String>					ExpectedRoomMealPlan			= null;
	private String								ErrorText							= "";
	private boolean								IsError							= false;
		
	/**
	 * z
	 * 
	 * @return the availabitlyCancerlatioPolicy
	 */
	public String getAvailabitlyCancerlatioPolicy() {
		return AvailabitlyCancerlatioPolicy;
	}

	public AvailabilityRequest getDefualtValues() {

		Room r1 = new Room();
		AvailabilityRequest requestObj = new AvailabilityRequest();
		requestObj.setAvailabitlyCancerlatioPolicy("Y");
		requestObj.setAvailabitlyHotelFee("Y");
		requestObj.setUserName(UserName);
		requestObj.setPassword(Password);
		requestObj.setCheckin("24-MAR-2018");
		requestObj.setCheckout("25-MAR-2018");
		requestObj.setNoofNights("1");
		requestObj.setCountry("US");
		requestObj.setState("NY");
		requestObj.setSearchType("hotelcode");
		requestObj.setCode(Code);
		r1.setRoomTypeID("0");
		r1.setBedTypeID("0");
		r1.setAdultsCount("2");
		r1.setChildCount("1");
		String[] childAges1 = new String[1];
		childAges1[0] = "9";
		r1.setChildAges(childAges1);
		ArrayList<Room> roomList = new ArrayList<Room>();
		roomList.add(r1);
		requestObj.setNoOfRooms("1");
		requestObj.setRoomlist(roomList);

		return requestObj;
	}

	/**
	 * @param availabitlyCancerlatioPolicy
	 *            the availabitlyCancerlatioPolicy to set
	 */
	public void setAvailabitlyCancerlatioPolicy(String availabitlyCancerlatioPolicy) {
		AvailabitlyCancerlatioPolicy = availabitlyCancerlatioPolicy;
	}

	/**
	 * @return the availabitlyHotelFee
	 */
	public String getAvailabitlyHotelFee() {
		return AvailabitlyHotelFee;
	}

	/**
	 * @param availabitlyHotelFee
	 *            the availabitlyHotelFee to set
	 */
	public void setAvailabitlyHotelFee(String availabitlyHotelFee) {
		AvailabitlyHotelFee = availabitlyHotelFee;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return UserName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		UserName = userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return Password;
	}

	/**
	 * @param password
	 *            the password to set
	 */
	public void setPassword(String password) {
		Password = password;
	}

	/**
	 * @return the checkin
	 */
	public String getCheckin() {
		return Checkin;
	}

	/**
	 * @param checkin
	 *            the checking to set
	 */
	public void setCheckin(String checkin) {
		Checkin = checkin;
	}

	/**
	 * @return the checkout
	 */
	public String getCheckout() {
		return Checkout;
	}

	/**
	 * @param checkout
	 *            the checkout to set
	 */
	public void setCheckout(String checkout) {
		Checkout = checkout;
	}

	/**
	 * @return the noofRooms
	 */
	public String getNoofNights() {
		return NoofNights;
	}

	/**
	 * @param noofNights
	 *            the noofNights to set
	 */
	public void setNoofNights(String noofNights) {
		NoofNights = noofNights;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return Country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		Country = country;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return State;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		State = state;
	}

	/**
	 * @return the hotelCode
	 */
	public String[] getCode() {
		return Code;
	}

	/**
	 * @param hotelCode
	 *            the hotelCode to set
	 */
	public void setCode(String[] Code) {
		this.Code = Code;
	}

	/**
	 * @return the roomlist
	 */
	public ArrayList<Room> getRoomlist() {
		return Roomlist;
	}

	/**
	 * @param roomlist
	 *            the roomlist to set
	 */
	public void setRoomlist(ArrayList<Room> roomlist) {
		Roomlist = roomlist;
	}

	public SearchBy getSearchType() {
		return SearchType;
	}

	public void setSearchType(SearchBy searchType) {
		SearchType = searchType;
	}

	public void setSearchType(String type) {

		SearchType = SearchBy.getSearchByType(type);
	}

	/**
	 * @return the noOfRooms
	 */
	public String getNoOfRooms() {
		return NoOfRooms;
	}

	/**
	 * @param noOfRooms
	 *            the noOfRooms to set
	 */
	public void setNoOfRooms(String noOfRooms) {
		NoOfRooms = noOfRooms;
	}

	/**
	 * @return the scenarioID
	 */
	public String getScenarioID() {
		return ScenarioID;
	}

	/**
	 * @param scenarioID the scenarioID to set
	 */
	public void setScenarioID(String scenarioID) {
		ScenarioID = scenarioID;
	}

	/**
	 * @return the expectedTax
	 */
	public ArrayList<String> getExpectedTax() {
		return ExpectedTax;
	}

	/**
	 * @param expectedTax the expectedTax to set
	 */
	public void setExpectedTax(ArrayList<String> expectedTax) {
		ExpectedTax = expectedTax;
	}

	/**
	 * @return the expectedTotal
	 */
	public ArrayList<String> getExpectedTotal() {
		return ExpectedTotal;
	}

	/**
	 * @param expectedTotal the expectedTotal to set
	 */
	public void setExpectedTotal(ArrayList<String> expectedTotal) {
		ExpectedTotal = expectedTotal;
	}

	/**
	 * @return the expectedRoomMealPlan
	 */
	public ArrayList<String> getExpectedRoomMealPlan() {
		return ExpectedRoomMealPlan;
	}

	/**
	 * @param expectedRoomMealPlan the expectedRoomMealPlan to set
	 */
	public void setExpectedRoomMealPlan(ArrayList<String> expectedRoomMealPlan) {
		ExpectedRoomMealPlan = expectedRoomMealPlan;
	}
	
	public String getErrorText() {
		return ErrorText;
	}

	public void setErrorText(String error) {
		ErrorText = error;
	}

	public boolean isIsError() {
		return IsError;
	}

	public void setIsError(boolean isError) {
		IsError = isError;
	}

}
