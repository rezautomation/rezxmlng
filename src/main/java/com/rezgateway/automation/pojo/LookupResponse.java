package com.rezgateway.automation.pojo;

import java.util.ArrayList;

public class LookupResponse {
	
	private String Referenceno 				= null;
	private String TourOperatorName 		= null;
	private String TourOperatorOrderNumber 	= null;
	private String HotelCode 				= null;
	private String HotelName 				= null;
	private String ReservationSource 		= null;
	private String ReservationStatus 		= null;
	private String CancellationNo 			= null;
	private String CancellationFee 			= null;
	private String CancellationTimeStamp 	= null;
	private String ConfirmationType 		= null;
	private String ReservationTimeStamp 	= null;
	private String ModificationTimeStamp 	= null;
	private String CheckIn 					= null;
	private String CheckOut 				= null;
	private String NoOfRooms 				= null;
	private String NoOfNights 				= null;
	private String TotNoOfAdults			= null;
	private String TotNoOfChildren			= null;
	private String Currency 				= null;
	private String Total 					= null;
	private String TotalTax 				= null;
	private String CustomerComment 			= null;
	private ArrayList<Room> Roomlist		= new ArrayList<Room>();
	
	public String getReferenceno() {
		return Referenceno;
	}
	public void setReferenceno(String referenceno) {
		Referenceno = referenceno;
	}
	public String getTourOperatorName() {
		return TourOperatorName;
	}
	public void setTourOperatorName(String tourOperatorName) {
		TourOperatorName = tourOperatorName;
	}
	public String getTourOperatorOrderNumber() {
		return TourOperatorOrderNumber;
	}
	public void setTourOperatorOrderNumber(String tourOperatorOrderNumber) {
		TourOperatorOrderNumber = tourOperatorOrderNumber;
	}
	
	public String getHotelCode() {
		return HotelCode;
	}
	public void setHotelCode(String hotelCode) {
		HotelCode = hotelCode;
	}
	public String getHotelName() {
		return HotelName;
	}
	public void setHotelName(String hotelName) {
		HotelName = hotelName;
	}
	public String getReservationSource() {
		return ReservationSource;
	}
	public void setReservationSource(String reservationSource) {
		ReservationSource = reservationSource;
	}
	public String getReservationStatus() {
		return ReservationStatus;
	}
	public void setReservationStatus(String reservationStatus) {
		ReservationStatus = reservationStatus;
	}
	public String getCancellationNo() {
		return CancellationNo;
	}
	public void setCancellationNo(String cancellationNo) {
		CancellationNo = cancellationNo;
	}
	public String getCancellationFee() {
		return CancellationFee;
	}
	public void setCancellationFee(String cancellationFee) {
		CancellationFee = cancellationFee;
	}
	public String getCancellationTimeStamp() {
		return CancellationTimeStamp;
	}
	public void setCancellationTimeStamp(String cancellationTimeStamp) {
		CancellationTimeStamp = cancellationTimeStamp;
	}
	public String getConfirmationType() {
		return ConfirmationType;
	}
	public void setConfirmationType(String confirmationType) {
		ConfirmationType = confirmationType;
	}
	public String getReservationTimeStamp() {
		return ReservationTimeStamp;
	}
	public void setReservationTimeStamp(String reservationTimeStamp) {
		ReservationTimeStamp = reservationTimeStamp;
	}
	public String getModificationTimeStamp() {
		return ModificationTimeStamp;
	}
	public void setModificationTimeStamp(String modificationTimeStamp) {
		ModificationTimeStamp = modificationTimeStamp;
	}
	public String getCheckIn() {
		return CheckIn;
	}
	public void setCheckIn(String checkIn) {
		CheckIn = checkIn;
	}
	public String getCheckOut() {
		return CheckOut;
	}
	public void setCheckOut(String checkOut) {
		CheckOut = checkOut;
	}
	public String getNoOfRooms() {
		return NoOfRooms;
	}
	public void setNoOfRooms(String noOfRooms) {
		NoOfRooms = noOfRooms;
	}
	public String getNoOfNights() {
		return NoOfNights;
	}
	public void setNoOfNights(String noOfNights) {
		NoOfNights = noOfNights;
	}
	public String getTotNoOfAdults() {
		return TotNoOfAdults;
	}
	public void setTotNoOfAdults(String totNoOfAdults) {
		TotNoOfAdults = totNoOfAdults;
	}
	public String getTotNoOfChildren() {
		return TotNoOfChildren;
	}
	public void setTotNoOfChildren(String totNoOfChildren) {
		TotNoOfChildren = totNoOfChildren;
	}
	public String getCurrency() {
		return Currency;
	}
	public void setCurrency(String currency) {
		Currency = currency;
	}
	public String getTotal() {
		return Total;
	}
	public void setTotal(String total) {
		Total = total;
	}
	public String getTotalTax() {
		return TotalTax;
	}
	public void setTotalTax(String totalTax) {
		TotalTax = totalTax;
	}
	public String getCustomerComment() {
		return CustomerComment;
	}
	public void setCustomerComment(String customerComment) {
		CustomerComment = customerComment;
	}
	public ArrayList<Room> getRoomlist() {
		return Roomlist;
	}
	public void setRoomlist(ArrayList<Room> roomlist) {
		Roomlist = roomlist;
	}
}
