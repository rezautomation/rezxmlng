package com.rezgateway.automation.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.rezgateway.automation.enu.ConfirmationType;

public class Room {
	
	private String 							RoomNo									= "";
	private String 							RoomCode								= "";
	private String 							RoomTypeID 								= "0";
	private String							RoomType								= "";
	private String							RoomDescription							= "";
	private String 							BedTypeID 								= "0";
	private String 							BedType									= "";
	private String 							RatePlanCode							= null;
	private String 							AdultsCount 							= null;
	private String 							ChildCount 								= null;
	private String[] 						ChildAges 								= {};
	private String[] 						AdultsOccpancy							= {};
	private String[] 						ChildOccupancy							= {};
	private String      					PromotionCode							= "";
	private ConfirmationType 				ConType									= ConfirmationType.CON;
	private String							ConfirmationConditions					= "";	
	private ArrayList<BookingPolicy> 		RoomPolicy 								= new ArrayList<BookingPolicy>();
	private Map<String, RateplansInfo> 		RatesPlanInfo 							= new HashMap<String,RateplansInfo>();
	private String 							RoomReferenceNo							= null;
	private String 							RoomResNo								= null;
	private ArrayList<HotelFees> 			LookupHotelFee							= new ArrayList<HotelFees>(); 
	
	/**
	 * @return the roomTypeID
	 */
	public String getRoomTypeID() {
		return RoomTypeID;
	}

	/**
	 * @param roomTypeID
	 *            the roomTypeID to set
	 */
	public void setRoomTypeID(String roomTypeID) {
		RoomTypeID = roomTypeID;
	}

	/**
	 * @return the bedTypeID
	 */
	public String getBedTypeID() {
		return BedTypeID;
	}

	/**
	 * @param bedTypeID
	 *            the bedTypeID to set
	 */
	public void setBedTypeID(String bedTypeID) {
		BedTypeID = bedTypeID;
	}

	/**
	 * @return the adultsCount
	 */
	public String getAdultsCount() {
		return AdultsCount;
	}

	/**
	 * @param adultsCount
	 *            the adultsCount to set
	 */
	public void setAdultsCount(String adultsCount) {
		AdultsCount = adultsCount;
	}

	/**
	 * @return the childCount
	 */
	public String getChildCount() {

		return ChildCount;

	}

	/**
	 * @param childCount
	 *            the childCount to set
	 */
	public void setChildCount(String childCount) {
		ChildCount = childCount;
	}

	/**
	 * @return the childAges
	 */
	public String[] getChildAges() {
		return ChildAges;
	}

	/**
	 * @param childAges
	 *            the childAges to set
	 */
	public void setChildAges(String[] childAges) {
		ChildAges = childAges;
	}

	public String[] getAdultsOccpancy() {
		return AdultsOccpancy;
	}

	public void setAdultsOccpancy(String[] adultsOccpancy) {
		AdultsOccpancy = adultsOccpancy;
	}

	/**
	 * @return the occpancyLastName
	 */
	public String[] getChildOccupancy() {
		return ChildOccupancy;
	}

	/**
	 * @param occpancyLastName
	 *            the occpancyLastName to set
	 */
	public void setChildOccupancy(String[] childOccupancy) {
		ChildOccupancy = childOccupancy;
	}

	/**
	 * @return the roomNo
	 */
	public String getRoomNo() {
		return RoomNo;
	}

	/**
	 * @param roomNo the roomNo to set
	 */
	public void setRoomNo(String roomNo) {
		RoomNo = roomNo;
	}

	/**
	 * @return the roomCode
	 */
	public String getRoomCode() {
		return RoomCode;
	}

	/**
	 * @param roomCode the roomCode to set
	 */
	public void setRoomCode(String roomCode) {
		RoomCode = roomCode;
	}

	/**
	 * @return the ratePlanCode
	 */
	public String getRatePlanCode() {
		return RatePlanCode;
	}

	/**
	 * @param ratePlanCode the ratePlanCode to set
	 */
	public void setRatePlanCode(String ratePlanCode) {
		RatePlanCode = ratePlanCode;
	}

	/**
	 * @return the ratesPlanInfo
	 */
	public Map<String, RateplansInfo> getRatesPlanInfo() {
		return RatesPlanInfo;
	}

	/**
	 * @param ratesPlanInfo the ratesPlanInfo to set
	 */
	public void setRatesPlanInfo(Map<String, RateplansInfo> ratesPlanInfo) {
		RatesPlanInfo = ratesPlanInfo;
	}

	/**
	 * @return the promotionCode
	 */
	public String getPromotionCode() {
		return PromotionCode;
	}

	/**
	 * @param promotionCode the promotionCode to set
	 */
	public void setPromotionCode(String promotionCode) {
		PromotionCode = promotionCode;
	}
	/**
	 * @return the roomPolicy
	 */
	public ArrayList<BookingPolicy> getRoomPolicy() {
		return RoomPolicy;
	}

	/**
	 * @param roomPolicy the roomPolicy to set
	 */
	public void setRoomPolicy(ArrayList<BookingPolicy> roomPolicy) {
		RoomPolicy = roomPolicy;
	}

	/**
	 * @return the conType
	 */
	public ConfirmationType getConType() {
		return ConType;
	}

	/**
	 * @param conType the conType to set
	 */
	public void setConType(ConfirmationType conType) {
		ConType = conType;
	}
	
	public void setConType(String type){
		ConType = ConfirmationType.getConfirmationType(type);
	}

	/**
	 * @return the roomType
	 */
	public String getRoomType() {
		return RoomType;
	}

	/**
	 * @param roomType the roomType to set
	 */
	public void setRoomType(String roomType) {
		RoomType = roomType;
	}

	/**
	 * @return the roomDescription
	 */
	public String getRoomDescription() {
		return RoomDescription;
	}

	/**
	 * @param roomDescription the roomDescription to set
	 */
	public void setRoomDescription(String roomDescription) {
		RoomDescription = roomDescription;
	}

	/**
	 * @return the bedType
	 */
	public String getBedType() {
		return BedType;
	}

	/**
	 * @param bedType the bedType to set
	 */
	public void setBedType(String bedType) {
		BedType = bedType;
	}

	/**
	 * @return the confirmationConditions
	 */
	public String getConfirmationConditions() {
		return ConfirmationConditions;
	}

	/**
	 * @param confirmationConditions the confirmationConditions to set
	 */
	public void setConfirmationConditions(String confirmationConditions) {
		ConfirmationConditions = confirmationConditions;
	}

	/**
	 * @return the roomReferenceNo
	 */
	public String getRoomReferenceNo() {
		return RoomReferenceNo;
	}

	/**
	 * @param roomReferenceNo the roomReferenceNo to set
	 */
	public void setRoomReferenceNo(String roomReferenceNo) {
		RoomReferenceNo = roomReferenceNo;
	}

	/**
	 * @return the roomResNo
	 */
	public String getRoomResNo() {
		return RoomResNo;
	}

	/**
	 * @param roomResNo the roomResNo to set
	 */
	public void setRoomResNo(String roomResNo) {
		RoomResNo = roomResNo;
	}

	public ArrayList<HotelFees> getLookupHotelFee() {
		return LookupHotelFee;
	}

	public void setLookupHotelFee(ArrayList<HotelFees> lookupHotelFee) {
		LookupHotelFee = lookupHotelFee;
	}
}
