package com.rezgateway.automation.pojo;

public class SynxisCancellationPolicy {

	private String policyCode			= null;
	private String penltyDesc			= null;
	private String amount				= null;
	private String isTaxInclusive		= null;
	private String deadLine				= null;
	private String offsetTimeUnit		= null;
	private String offsetUnitMultiplier	= null;
	private String offsetDropTime 		= null;
	
	public String getPolicyCode() {
		return policyCode;
	}
	public void setPolicyCode(String policyCode) {
		this.policyCode = policyCode;
	}
	public String getPenltyDesc() {
		return penltyDesc;
	}
	public void setPenltyDesc(String penltyDesc) {
		this.penltyDesc = penltyDesc;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getIsTaxInclusive() {
		return isTaxInclusive;
	}
	public void setIsTaxInclusive(String isTaxInclusive) {
		this.isTaxInclusive = isTaxInclusive;
	}
	public String getDeadLine() {
		return deadLine;
	}
	public void setDeadLine(String deadLine) {
		this.deadLine = deadLine;
	}
	public String getOffsetTimeUnit() {
		return offsetTimeUnit;
	}
	public void setOffsetTimeUnit(String offsetTimeUnit) {
		this.offsetTimeUnit = offsetTimeUnit;
	}
	public String getOffsetUnitMultiplier() {
		return offsetUnitMultiplier;
	}
	public void setOffsetUnitMultiplier(String offsetUnitMultiplier) {
		this.offsetUnitMultiplier = offsetUnitMultiplier;
	}
	public String getOffsetDropTime() {
		return offsetDropTime;
	}
	public void setOffsetDropTime(String offsetDropTime) {
		this.offsetDropTime = offsetDropTime;
	}
	

}
