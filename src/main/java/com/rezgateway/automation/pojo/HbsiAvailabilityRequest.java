package com.rezgateway.automation.pojo;

import java.util.HashMap;
import java.util.Map;

public class HbsiAvailabilityRequest {

	private String 			username 		= null;
	private String 			password 		= null;
	private String 			checkin 		= null;
	private String 			checkout 		= null;
	private Map<String, String[]> ratePlanCandidate			= new HashMap<String, String[]>();
	private Map<String, String[]> roomStayCandidate			= new HashMap<String, String[]>();
	
	public String getUsername(){
		return username;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
	public String getCheckin(){
		return checkin;
	}
	
	public void setCheckin(String checkin){
		this.checkin = checkin;
	}
	
	public String getCheckout(){
		return checkout;
	}
	
	public void setCheckout(String checkout){
		this.checkout = checkout;
	}
	
	public Map<String, String[]> getRatePlanCandidate() {
		return ratePlanCandidate;
	}

	public void setRatePlanCandidate(Map<String, String[]> ratePlanCandidate) {
		this.ratePlanCandidate = ratePlanCandidate;
	}

	public Map<String, String[]> getRoomStayCandidate() {
		return roomStayCandidate;
	}

	public void setRoomStayCandidate(Map<String, String[]> roomStayCandidate) {
		this.roomStayCandidate = roomStayCandidate;
	}
}
