package com.rezgateway.automation.pojo;

import java.util.ArrayList;

public class HbsiRoom {
	private String hotelCode		= null;
	private String roomTypeCode		= null;
	private String ratePlanCode		= null;
	private String totalBeforeTax	= null;
	private String totalAfterTax	= null;
	private String adultCount	= null;
	private String childCount	= null;
	private ArrayList<HbsiCancellationPolicy> cp 	= new ArrayList<HbsiCancellationPolicy>();	
	
	
	public String getHotelCode() {
		return hotelCode;
	}
	public void setHotelCode(String hotelCode) {
		this.hotelCode = hotelCode;
	}	
	public String getTotalBeforeTax() {
		return totalBeforeTax;
	}
	public void setTotalBeforeTax(String totalBeforeTax) {
		this.totalBeforeTax = totalBeforeTax;
	}
	public String getTotalAfterTax() {
		return totalAfterTax;
	}
	public void setTotalAfterTax(String totalAfterTax) {
		this.totalAfterTax = totalAfterTax;
	}
	public String getRoomTypeCode() {
		return roomTypeCode;
	}
	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}
	public String getRatePlanCode() {
		return ratePlanCode;
	}
	public void setRatePlanCode(String ratePlanCode) {
		this.ratePlanCode = ratePlanCode;
	}
	public ArrayList<HbsiCancellationPolicy> getCp() {
		return cp;
	}
	public void setCp(ArrayList<HbsiCancellationPolicy> cp) {
		this.cp = cp;
	}
	public String getAdultCount() {
		return adultCount;
	}
	public void setAdultCount(String adultCount) {
		this.adultCount = adultCount;
	}
	public String getChildCount() {
		return childCount;
	}
	public void setChildCount(String childCount) {
		this.childCount = childCount;
	}
	

}
