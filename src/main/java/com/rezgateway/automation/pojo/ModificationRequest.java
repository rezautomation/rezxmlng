package com.rezgateway.automation.pojo;

import java.util.ArrayList;

import com.rezgateway.automation.enu.ModificationType;

public class ModificationRequest extends ReservationResponse {
	
	private String              ScenarioID 								= null;
	private String				Version									= "2.0";			
	private String 				UserName 								= "TestDynamicRateTO";
	private String 				Password 								= "ty5@test12345";
	private String				ModificationDetailsTimeStamp			= null;
	private String				NoOfRooms								= null;
	private String				NumOFDays								= null;
	private ModificationType	ModifyType								= ModificationType.GUESTDETAILS;
	private String 				UserMODComment							= "";
	private String 				HotelMODComment							= "";
	private String				NewToOrderNo							= null;
	private String 				ModCheckin 								= null;
	private String 				ModCheckout 							= null;
	private String 				ModTotal 								= null;
	private String 				ModTotalTax 							= null;
	
	
	public ModificationRequest getDefaultValue(){
		
		ModificationRequest reqObJ = new ModificationRequest();
		Room r1 = new Room();
		reqObJ.setUserName("TesthotelName");
		reqObJ.setPassword("Akila");
		reqObJ.setModificationDetailsTimeStamp("20090925T11:19:36");
		reqObJ.setReferenceno("362528317X");
		reqObJ.setConfType(getConfType());
		reqObJ.setModifyType("5");
		reqObJ.setTourOperatorOrderNumber("20113217T03:32:20");
		reqObJ.setCheckin("24-MAR-2018");
		reqObJ.setCheckout("25-MAR-2018");
		reqObJ.setNoofNights("1");
		String[] Code = { "6109" };
		reqObJ.setCode(Code);
		reqObJ.setCurrency("USD");
		reqObJ.setTotal("0");
		reqObJ.setTotalTax("0");

		r1.setRoomNo("1");
		r1.setRoomCode("99171");
		r1.setRoomTypeID("3");
		r1.setRatePlanCode("1");
		r1.setBedTypeID("4");
		r1.setAdultsCount("2");
		r1.setChildCount("1");
		String[] namearray1 = new String[2];
		namearray1[0] = "Mr_Akila_Change Samaranayake";
		namearray1[1] = "MS_Rasoja_Change Samaranayake";
		r1.setAdultsOccpancy(namearray1);
		String[] namearray2 = new String[1];
		namearray2[0] = "Child_Sumiuru_Change Gihan";
		r1.setChildOccupancy(namearray2);
		String[] childAges1 = new String[1];
		childAges1[0] = "9";
		r1.setChildAges(childAges1);
		ArrayList<Room> roomList = new ArrayList<Room>();
		roomList.add(r1);
		reqObJ.setRoomlist(roomList);
		reqObJ.setUserComment("new Comment Teseting ");
		return reqObJ;
	}
	

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return UserName;
	}
	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		UserName = userName;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return Password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		Password = password;
	}
	
	/**
	 * @return the userComment
	 */
	public String getUserComment() {
		return UserMODComment;
	}
	/**
	 * @param userComment the userComment to set
	 */
	public void setUserComment(String userComment) {
		UserMODComment = userComment;
	}
	/**
	 * @return the hotelComment
	 */
	public String getHotelComment() {
		return HotelMODComment;
	}
	/**
	 * @param hotelComment the hotelComment to set
	 */
	public void setHotelComment(String hotelComment) {
		HotelMODComment = hotelComment;
	}
	/**
	 * @return the modifyType
	 */
	public ModificationType getModifyType() {
		return ModifyType;
	}
	/**
	 * @param modifyType the modifyType to set
	 */
	public void setModifyType(String modifyType) {
		ModifyType = ModificationType.getModifyType(modifyType);
	}
	/**
	 * @return the modificationDetailsTimeStamp
	 */
	public String getModificationDetailsTimeStamp() {
		return ModificationDetailsTimeStamp;
	}
	/**
	 * @param modificationDetailsTimeStamp the modificationDetailsTimeStamp to set
	 */
	public void setModificationDetailsTimeStamp(String modificationDetailsTimeStamp) {
		ModificationDetailsTimeStamp = modificationDetailsTimeStamp;
	}
	/**
	 * @param modifyType the modifyType to set
	 */
	public void setModifyType(ModificationType modifyType) {
		this.ModifyType = modifyType;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return Version;
	}
	
	/**
	 * @param version the version to set
	 */
	
	public void setVersion(String version) {
		Version = version;
	}

	/**
	 * @return the scenarioID
	 */
	
	public String getScenarioID() {
		return ScenarioID;
	}

	/**
	 * @param scenarioID the scenarioID to set
	 */
	public void setScenarioID(String scenarioID) {
		ScenarioID = scenarioID;
	}


	/**
	 * @return the numOFDays
	 */
	public String getNumOFDays() {
		return NumOFDays;
	}


	/**
	 * @param numOFDays the numOFDays to set
	 */
	public void setNumOFDays(String numOFDays) {
		NumOFDays = numOFDays;
	}

	public String getNewToOrderNo() {
		return NewToOrderNo;
	}


	public void setNewToOrderNo(String newToOrderNo) {
		NewToOrderNo = newToOrderNo;
	}


	public String getUserMODComment() {
		return UserMODComment;
	}


	public void setUserMODComment(String userMODComment) {
		UserMODComment = userMODComment;
	}


	public String getHotelMODComment() {
		return HotelMODComment;
	}


	public void setHotelMODComment(String hotelMODComment) {
		HotelMODComment = hotelMODComment;
	}


	public String getNoOfRooms() {
		return NoOfRooms;
	}


	public void setNoOfRooms(String noOfRooms) {
		NoOfRooms = noOfRooms;
	}


	public String getModCheckin() {
		return ModCheckin;
	}


	public void setModCheckin(String modCheckin) {
		ModCheckin = modCheckin;
	}


	public String getModCheckout() {
		return ModCheckout;
	}


	public void setModCheckout(String modCheckout) {
		ModCheckout = modCheckout;
	}


	public String getModTotal() {
		return ModTotal;
	}


	public void setModTotal(String modTotal) {
		ModTotal = modTotal;
	}


	public String getModTotalTax() {
		return ModTotalTax;
	}


	public void setModTotalTax(String modTotalTax) {
		ModTotalTax = modTotalTax;
	}
	
}
