/**
 * 
 */
package com.rezgateway.automation.pojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Dinethra
 * 
 */
public class SynxisReservationResponse {

	private String resResponseType = null;
	private String supplierID = null;
	private String sidContext = null;
	private String currencyCode = null;
	private String roomTypeCode = null;
	private int numberOfUnits = 0;
	private String roomDesciption = null;
	private int roomMaxOccupancy = 0;
	private String ratePlanCode = null;
	private String ratePlanName = null;
	private String guranteeCode = null;
	private double baseAmountBeforeTax = 0.0;
	private double baseAmountAfterTax = 0.0;
	private double discountAmountBeforeTax = 0.0;
	private double discountAmountAfterTax = 0.0;
	private double feeAmount = 0.0;
	private Map<String, String[]> nightlyRate = new HashMap<String, String[]>();
	private Map<String, String[]> discountNightlyRate = new HashMap<String, String[]>();
	private int adultCount = 0;
	private int childCount = 0;
	private String checkin = null;
	private String checkout = null;
	private String duration = null;
	private int hotelCode = 0;
	private String hotelName = null;
	private ArrayList<Occupancy> occupant = new ArrayList<Occupancy>();
	private double totalBeforeTax = 0.0;
	private double totalAfterTax = 0.0;
	private double tax = 0.0;
	private String cardcode = null;
	private String cardNumber = null;
	private String expireDate = null;
	private String cardHolderName = null;
	private ArrayList<String> hotelReservationID = new ArrayList<String>();
	private SynxisCancellationPolicy sCancelPolicy = new SynxisCancellationPolicy();

	public String getResResponseType() {
		return resResponseType;
	}

	public void setResResponseType(String resResponseType) {
		this.resResponseType = resResponseType;
	}

	public String getSupplierID() {
		return supplierID;
	}

	public void setSupplierID(String supplierID) {
		this.supplierID = supplierID;
	}

	public String getSidContext() {
		return sidContext;
	}

	public void setSidContext(String sidContext) {
		this.sidContext = sidContext;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getRoomTypeCode() {
		return roomTypeCode;
	}

	public void setRoomTypeCode(String roomTypeCode) {
		this.roomTypeCode = roomTypeCode;
	}

	public int getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(int numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public String getRoomDesciption() {
		return roomDesciption;
	}

	public void setRoomDesciption(String roomDesciption) {
		this.roomDesciption = roomDesciption;
	}

	public String getRatePlanCode() {
		return ratePlanCode;
	}

	public void setRatePlanCode(String ratePlanCode) {
		this.ratePlanCode = ratePlanCode;
	}

	public String getRatePlanName() {
		return ratePlanName;
	}

	public void setRatePlanName(String ratePlanName) {
		this.ratePlanName = ratePlanName;
	}

	public String getGuranteeCode() {
		return guranteeCode;
	}

	public void setGuranteeCode(String guranteeCode) {
		this.guranteeCode = guranteeCode;
	}

	public double getBaseAmountBeforeTax() {
		return baseAmountBeforeTax;
	}

	public void setBaseAmountBeforeTax(double baseAmountBeforeTax) {
		this.baseAmountBeforeTax = baseAmountBeforeTax;
	}

	public double getBaseAmountAfterTax() {
		return baseAmountAfterTax;
	}

	public void setBaseAmountAfterTax(double baseAmountAfterTax) {
		this.baseAmountAfterTax = baseAmountAfterTax;
	}

	public double getDiscountAmountBeforeTax() {
		return discountAmountBeforeTax;
	}

	public void setDiscountAmountBeforeTax(double discountAmountBeforeTax) {
		this.discountAmountBeforeTax = discountAmountBeforeTax;
	}

	public double getDiscountAmountAfterTax() {
		return discountAmountAfterTax;
	}

	public void setDiscountAmountAfterTax(double discountAmountAfterTax) {
		this.discountAmountAfterTax = discountAmountAfterTax;
	}

	public double getFeeAmount() {
		return feeAmount;
	}

	public void setFeeAmount(double feeAmount) {
		this.feeAmount = feeAmount;
	}

	public Map<String, String[]> getNightlyRate() {
		return nightlyRate;
	}

	public void setNightlyRate(Map<String, String[]> nightlyRate) {
		this.nightlyRate = nightlyRate;
	}

	public Map<String, String[]> getDiscountNightlyRate() {
		return discountNightlyRate;
	}

	public void setDiscountNightlyRate(Map<String, String[]> discountNightlyRate) {
		this.discountNightlyRate = discountNightlyRate;
	}

	public int getAdultCount() {
		return adultCount;
	}

	public void setAdultCount(int adultCount) {
		this.adultCount = adultCount;
	}

	public int getChildCount() {
		return childCount;
	}

	public void setChildCount(int childCount) {
		this.childCount = childCount;
	}

	public String getCheckin() {
		return checkin;
	}

	public void setCheckin(String checkin) {
		this.checkin = checkin;
	}

	public String getCheckout() {
		return checkout;
	}

	public void setCheckout(String checkout) {
		this.checkout = checkout;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public int getHotelCode() {
		return hotelCode;
	}

	public void setHotelCode(int hotelCode) {
		this.hotelCode = hotelCode;
	}

	public String getHotelName() {
		return hotelName;
	}

	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}

	public double getTotalBeforeTax() {
		return totalBeforeTax;
	}

	public void setTotalBeforeTax(double totalBeforeTax) {
		this.totalBeforeTax = totalBeforeTax;
	}

	public double getTotalAfterTax() {
		return totalAfterTax;
	}

	public void setTotalAfterTax(double totalAfterTax) {
		this.totalAfterTax = totalAfterTax;
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

	public String getCardcode() {
		return cardcode;
	}

	public void setCardcode(String cardcode) {
		this.cardcode = cardcode;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getCardHolderName() {
		return cardHolderName;
	}

	public void setCardHolderName(String cardHolderName) {
		this.cardHolderName = cardHolderName;
	}

	public ArrayList<String> getHotelReservationID() {
		return hotelReservationID;
	}

	public void setHotelReservationID(ArrayList<String> hotelReservationID) {
		this.hotelReservationID = hotelReservationID;
	}

	public int getRoomMaxOccupancy() {
		return roomMaxOccupancy;
	}

	public void setRoomMaxOccupancy(int roomMaxOccupancy) {
		this.roomMaxOccupancy = roomMaxOccupancy;
	}

	public SynxisCancellationPolicy getsCancelPolicy() {
		return sCancelPolicy;
	}

	public void setsCancelPolicy(SynxisCancellationPolicy sCancelPolicy) {
		this.sCancelPolicy = sCancelPolicy;
	}

	public ArrayList<Occupancy> getOccupant() {
		return occupant;
	}

	public void setOccupant(ArrayList<Occupancy> occupant) {
		this.occupant = occupant;
	}

}
