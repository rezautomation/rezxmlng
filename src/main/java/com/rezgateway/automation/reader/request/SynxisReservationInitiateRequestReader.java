/**
 * 
 */
package com.rezgateway.automation.reader.request;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom2.*;

import com.rezgateway.automation.pojo.Occupancy;
import com.rezgateway.automation.pojo.SynxisReservationInitiateRequest;

/**
 * @author Dinethra
 *
 */

public class SynxisReservationInitiateRequestReader {
	
	static Logger logger;
	
	Namespace ns = Namespace.getNamespace("http://www.w3.org/2003/05/soap-envelope");
	Namespace ns1 = Namespace.getNamespace("http://www.opentravel.org/OTA/2003/05");
	Namespace ns2 = Namespace.getNamespace("http://htng.org/1.1/Header/");
	
	public SynxisReservationInitiateRequestReader(){
		logger = Logger.getLogger(SynxisReservationInitiateRequestReader.class);
	}
	
	public SynxisReservationInitiateRequest RequestReader(Document doc){
		
		logger.info("Initializing request initiate reader with ==>" + doc.toString());
		
		SynxisReservationInitiateRequest srir = new SynxisReservationInitiateRequest();
		String ageQCode = null;
		Occupancy ocu = new Occupancy();
		
		Element root = doc.getRootElement();
		Element header = root.getChild("Header", ns);
		Element htng = header.getChild("HTNGHeader", ns2);
		Element from = htng.getChild("From", ns2);
		Element credentials = from.getChild("Credential", ns2);
		
		srir.setUsername(credentials.getChildText("userName", ns2));
		srir.setPassword(credentials.getChildText("password", ns2));
		
		Element body = root.getChild("Body", ns);
		Element ota_HotelResRQ = body.getChild("OTA_HotelResRQ", ns1);
		Element hReservations = ota_HotelResRQ.getChild("HotelReservations", ns1);
		Element hreserv = hReservations.getChild("HotelReservation", ns1);
		Element roomStays = hreserv.getChild("RoomStays", ns1);
		Element roomStay = roomStays.getChild("RoomStay", ns1);
		Element roomTypes = roomStay.getChild("RoomTypes", ns1);
		
		logger.info("Reading room and rate plan details");
		
		srir.setRoomCode(roomTypes.getChild("RoomType", ns1).getAttributeValue("RoomTypeCode"));
		srir.setRoomCount(Integer.parseInt(roomTypes.getChild("RoomType", ns1).getAttributeValue("NumberOfUnits")));
		
		Element ratePlans = roomStay.getChild("RatePlans", ns1);
		
		srir.setRatePlanCode(ratePlans.getChild("RatePlan", ns1).getAttributeValue("RatePlanCode"));
		
		logger.info("Reading guest count");
		
		Element guestCounts = roomStay.getChild("GuestCounts", ns1);
		List<Element> guestCount = guestCounts.getChildren("GuestCount", ns1);
 		
 		try {
			for (Element guest : guestCount) {
				ageQCode = guest.getAttributeValue("AgeQualifyingCode");

				if ("10".equals(ageQCode))
					srir.setAdultCount(Integer.parseInt(guest.getAttributeValue("Count")));
				else if ("8".equals(ageQCode))
					srir.setChildCount(Integer.parseInt(guest.getAttributeValue("Count")));
			}
		} catch (Exception e) {
			logger.fatal("Are qualifying code is wrong : " + e);
		}
 		
 		logger.info("Reading reservation details");
 		
		srir.setCheckin(roomStay.getChild("TimeSpan", ns1).getAttributeValue("Start"));
 		srir.setCheckout(roomStay.getChild("TimeSpan", ns1).getAttributeValue("End"));
 		srir.setHotelCode(roomStay.getChild("BasicPropertyInfo", ns1).getAttributeValue("HotelCode"));
 		
 		Element resGuests = hreserv.getChild("ResGuests", ns1);
 		List<Element> resGuest = resGuests.getChildren("ResGuest", ns1);
 		ArrayList<Occupancy> occupant = new ArrayList<Occupancy>();
 		
 		logger.info("Reading occupant details");
 		
 		try {
			for (Element guest : resGuest) {

				Element profiles = guest.getChild("Profiles", ns1);
				Element profileInfo = profiles.getChild("ProfileInfo", ns1);
				Element profile = profileInfo.getChild("Profile", ns1);
				Element customer = profile.getChild("Customer", ns1);
				Element personName = customer.getChild("PersonName", ns1);

				ocu.setNamePrefix(personName.getChildText("NamePrefix", ns1));
				ocu.setFirstName(personName.getChildText("GivenName", ns1));
				ocu.setLastName(personName.getChildText("Surname", ns1));

				Element telephone = customer.getChild("Telephone", ns1);

				ocu.setTelephone(telephone.getAttributeValue("PhoneNumber"));
				ocu.setEmail(customer.getChildText("Email", ns1));

				Element address = customer.getChild("Address", ns1);

				ocu.setAddressLine(address.getChildText("AddressLine", ns1));
				ocu.setCity(address.getChildText("CityName", ns1));
				ocu.setPostalCode(Integer.parseInt(address.getChildText("PostalCode", ns1)));
				ocu.setStateCode(address.getChild("StateProv", ns1).getAttributeValue("StateCode"));
				ocu.setCountryCode(address.getChild("CountryName", ns1).getAttributeValue("Code"));

				occupant.add(ocu);
			}
			srir.setOccupant(occupant);
		} catch (Exception e) {
			logger.fatal("Occupancy details not read properly : " + e);
		}
 		
 		logger.info("Reading credit card details");
 		
		Element resGlobalInfo = hreserv.getChild("ResGlobalInfo", ns1);
 		Element guarantee = resGlobalInfo.getChild("Guarantee", ns1);
 		Element guaranteesAccepted = guarantee.getChild("GuaranteesAccepted", ns1);
 		Element guaranteeAccepted = guaranteesAccepted.getChild("GuaranteeAccepted", ns1);
 		Element paymentCard = guaranteeAccepted.getChild("PaymentCard", ns1);
 		
 		srir.setCardcode(paymentCard.getAttributeValue("CardCode"));
 		srir.setCardNumber(paymentCard.getAttributeValue("CardNumber"));
 		srir.setSeriesCode(Integer.parseInt(paymentCard.getAttributeValue("SeriesCode")));
 		srir.setExpireDate(paymentCard.getAttributeValue("ExpireDate"));
 		srir.setCardHolderName(paymentCard.getChildText("CardHolderName", ns1));
		return srir;
	}

}
