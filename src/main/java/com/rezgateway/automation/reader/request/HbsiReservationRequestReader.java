package com.rezgateway.automation.reader.request;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import com.rezgateway.automation.pojo.HbsiReservationRequest;
import com.rezgateway.automation.pojo.HbsiRoom;
import com.rezgateway.automation.pojo.Occupancy;

public class HbsiReservationRequestReader {
	static Logger logger;
	
	public HbsiReservationRequestReader(){
		logger = Logger.getLogger(HbsiReservationRequestReader.class);
	}
	
	public HbsiReservationRequest RequestReader(Document doc) throws JDOMException, IOException{
		logger.info("Initializing request reader with ==>" + doc.toString());
		HbsiReservationRequest req = new HbsiReservationRequest();
		Element rootElem = doc.getRootElement();		
		
		Namespace ns = Namespace.getNamespace("http://www.w3.org/2003/05/soap-envelope");
		Namespace ns1 = Namespace.getNamespace("http://www.demandmatrix.net/HBSIXML4/");
		Namespace ns2 = Namespace.getNamespace("http://www.opentravel.org/OTA/2003/05");
		Element body = rootElem.getChild("Body", ns);
		
		Element xml = body.getChild("GetSoapRequest", ns1).getChild("Message", ns1).getChild("XML", ns1);
		String request = xml.getText();
		
		SAXBuilder builder = new SAXBuilder();
		InputStream stream = new ByteArrayInputStream(request.getBytes("UTF-8"));
		Document docReq = builder.build(stream);
		
		Element rootElemReq = docReq.getRootElement();
		Element hotelReservation = rootElemReq.getChild("HotelReservations", ns2).getChild("HotelReservation", ns2);
		String uniqueId = hotelReservation.getChild("UniqueID", ns2).getAttributeValue("ID");
		req.setUniqueId(uniqueId);
		
		List<Element> roomStayList = hotelReservation.getChild("RoomStays", ns2).getChildren("RoomStay", ns2);
		ArrayList<HbsiRoom> roomList = new ArrayList<HbsiRoom>();
		
		try {
			for (Element roomStay : roomStayList){
				String hotelCode = roomStay.getChild("BasicPropertyInfo", ns2).getAttributeValue("HotelCode");
								
				List<Element> roomRateList = roomStay.getChild("RoomRates", ns2).getChildren("RoomRate", ns2);
				HbsiRoom room = new HbsiRoom();
				
				try {
					for(Element roomRate : roomRateList){
						String roomTypeCode = roomRate.getAttributeValue("RoomTypeCode");
						String ratePlanCode = roomRate.getAttributeValue("RatePlanCode");
						room.setRoomTypeCode(roomTypeCode);
						room.setRatePlanCode(ratePlanCode);							
						
						Element rate = roomRate.getChild("Rates", ns2).getChild("Rate", ns2);
						String totalBeforeTax = rate.getChild("Total", ns2).getAttributeValue("AmountBeforeTax");
						String totalAfterTax = rate.getChild("Total", ns2).getAttributeValue("AmountAfterTax");
						room.setTotalBeforeTax(totalBeforeTax);
						room.setTotalAfterTax(totalAfterTax);
						room.setHotelCode(hotelCode);
						
						roomList.add(room);
						req.setRoomList(roomList);
					}					
					
				} catch (Exception e) {
					logger.error("Error in reading room rate details : "+ e);
				}
				
				List<Element> guestList = roomStay.getChild("GuestCounts", ns2).getChildren("GuestCount", ns2);
				logger.info("Reading occupancy count");				
				String adultCount = null;
				String childCount = null;
		 		
		 		try {
					for (Element guest : guestList) {
						String ageCode = guest.getAttributeValue("AgeQualifyingCode");

						if ("10".equals(ageCode)) {
							adultCount = guest.getAttributeValue("Count");							
						} else if ("8".equals(ageCode)) {
							childCount = guest.getAttributeValue("Count");							
						}
						room.setAdultCount(adultCount);
						room.setChildCount(childCount);
					}					
				} catch (Exception e) {
					logger.error("Error in reading occupancy details : " + e);
				}
		 		
		 		String checkIn = roomStay.getChild("TimeSpan", ns2).getAttributeValue("Start");
		 		String checkOut = roomStay.getChild("TimeSpan", ns2).getAttributeValue("End");
		 		req.setCheckin(checkIn);
		 		req.setCheckout(checkOut);
		 		
		 		Element depositPayments = hotelReservation.getChild("ResGlobalInfo", ns2).getChild("DepositPayments", ns2);
		 		Element acceptedPayments = depositPayments.getChild("RequiredPayment", ns2).getChild("AcceptedPayments", ns2);
		 		Element paymentCard = acceptedPayments.getChild("AcceptedPayment", ns2).getChild("PaymentCard", ns2);
		 		req.setCardCode(paymentCard.getAttributeValue("CardCode"));
		 		req.setCardType(paymentCard.getAttributeValue("CardType"));
		 		req.setExpireDate(paymentCard.getAttributeValue("ExpireDate"));
		 		req.setCardNo(paymentCard.getAttributeValue("CardNumber"));
		 		req.setSeriesCode(paymentCard.getAttributeValue("SeriesCode"));
		 		
		 		req.setCardHolderName(paymentCard.getChildText("CardHolderName", ns2));
		 		
		 		List<Element> resGuestList = hotelReservation.getChild("ResGuests", ns2).getChildren("ResGuest", ns2);
		 		ArrayList<Occupancy> occupancyList = new ArrayList<Occupancy>();
		 		Occupancy occupant = new Occupancy();
		 		
		 		try {
		 			for (Element resGuest : resGuestList){
//		 				String ageCode = resGuest.getAttributeValue("AgeQualifyingCode");
		 				Element customer = resGuest.getChild("Profiles", ns2).getChild("ProfileInfo", ns2).getChild("Profile", ns2).getChild("Customer", ns2);
		 				Element person = customer.getChild("PersonName", ns2);
		 				occupant.setFirstName(person.getChildText("GivenName", ns2));
		 				occupant.setLastName(person.getChildText("Surname", ns2));
		 				occupant.setNamePrefix(person.getChildText("NameTitle", ns2));
		 				occupant.setTelephone(customer.getChild("Telephone", ns2).getAttributeValue("PhoneNumber"));
		 				
		 				Element address = customer.getChild("Address", ns2);
		 				occupant.setAddressLine(address.getChildText("AddressLine", ns2));
		 				occupant.setCity(address.getChildText("CityName", ns2));
		 				occupant.setPostalCode(Integer.parseInt(address.getChildText("PostalCode", ns2)));
		 				occupant.setStateCode(address.getChild("StateProv", ns2).getAttributeValue("StateCode"));
		 				occupant.setCountryCode(address.getChild("CountryName", ns2).getAttributeValue("Code"));
		 				
		 				occupancyList.add(occupant);
		 				req.setOccupant(occupancyList);
		 			}
					
				} catch (Exception e) {
					logger.error("Error in reading guest detauls : " + e);
				}		 		
		 		
			}
		} catch (Exception e) {
			logger.error("Error in reading room stay details : " + e);
		}
				
		return req;
	}

}
