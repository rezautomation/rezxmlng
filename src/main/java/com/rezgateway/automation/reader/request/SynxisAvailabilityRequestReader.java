package com.rezgateway.automation.reader.request;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;

import com.rezgateway.automation.pojo.SynxisAvailabilityRequest;

public class SynxisAvailabilityRequestReader {
	
	static Logger logger;
	
	public SynxisAvailabilityRequestReader() {
		logger = Logger.getLogger(SynxisAvailabilityRequestReader.class);
	}
	
	public SynxisAvailabilityRequest RequestReader(Document doc) throws JDOMException, IOException {
		
		logger.info("Initializing response reader with ==>" + doc.toString());
		SynxisAvailabilityRequest req = new SynxisAvailabilityRequest();
		Element rootelem = doc.getRootElement();
		
		Namespace ns = Namespace.getNamespace("http://www.w3.org/2003/05/soap-envelope");
		Element header = rootelem.getChild("Header", ns);
		
 		Namespace ns1 = Namespace.getNamespace("http://htng.org/1.1/Header/");
 		Element htngHeader = header.getChild("HTNGHeader", ns1);
 		Element from = htngHeader.getChild("From", ns1);
 		Element credential = from.getChild("Credential", ns1); 		
 		
 		req.setUsername(credential.getChildText("userName", ns1));
 		req.setPassword(credential.getChildText("password", ns1));
 		
 		Element body = rootelem.getChild("Body", ns);
 		Namespace ns2 = Namespace.getNamespace("http://www.opentravel.org/OTA/2003/05");
 		Element OTA_HotelAvailRQ = body.getChild("OTA_HotelAvailRQ", ns2);
 		
 		String echoToken = OTA_HotelAvailRQ.getAttributeValue("EchoToken");
 		req.setEchoToken(echoToken);
 		
 		Element segments = OTA_HotelAvailRQ.getChild("AvailRequestSegments", ns2);
 		Element segment = segments.getChild("AvailRequestSegment", ns2);
 		
 		req.setCheckin(segment.getChild("StayDateRange", ns2).getAttributeValue("Start"));
 		req.setCheckout(segment.getChild("StayDateRange", ns2).getAttributeValue("End"));

 		List<Element> rateplanElements = segment.getChildren("RatePlanCandidates", ns2);
 		ArrayList<String> rateplanList = new ArrayList<String>();
 	
 		logger.info("Reading rate plan details");
 		
 		try {
			for (Element rateplan : rateplanElements) {
				String rate = rateplan.getChild("RatePlanCandidate", ns2).getAttributeValue("RatePlanCode");
				rateplanList.add(rate);
				req.setRateplan(rateplanList);
			}
		} catch (Exception e) {
			logger.error("Eror in reading rate plan details : " + e);
		}
 		
		Element roomStayCandidate = segment.getChild("RoomStayCandidates", ns2).getChild("RoomStayCandidate", ns2);
 		String roomQty = roomStayCandidate.getAttributeValue("Quantity");
 		req.setRoomCount(Integer.parseInt(roomQty));
 		
 		Element guests = roomStayCandidate.getChild("GuestCounts", ns2);
 		
 		List<Element> guestCount = guests.getChildren("GuestCount", ns2);
 		
 		logger.info("Reading occupancy count");
 		
 		try {
			for (Element guest : guestCount) {
				String ageCode = guest.getAttributeValue("AgeQualifyingCode");

				if ("10".equals(ageCode)) {
					int adultCount = Integer.parseInt(guest.getAttributeValue("Count"));
					req.setAdultCount(adultCount);
				} else if ("8".equals(ageCode)) {
					int childCount = Integer.parseInt(guest.getAttributeValue("Count"));
					req.setChildCount(childCount);
				}
			}
		} catch (Exception e) {
			logger.error("Error in reading occupancy details");
		}
 		
		Element criterion = segment.getChild("HotelSearchCriteria", ns2).getChild("Criterion", ns2);
 		String hotelcode = criterion.getChild("HotelRef", ns2).getAttributeValue("HotelCode");
 		req.setHotelcode(hotelcode);
 		 		
 		return req;
	
	}		
}
