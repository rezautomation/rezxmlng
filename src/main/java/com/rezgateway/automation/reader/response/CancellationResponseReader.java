package com.rezgateway.automation.reader.response;

import java.io.File;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.xml.sax.InputSource;

import com.rezgateway.automation.pojo.CancellationResponse;

public class CancellationResponseReader {

	private static Logger logger = null;

	public CancellationResponseReader() {

		logger = Logger.getLogger(this.getClass());
	}

	public CancellationResponse getResponse(String Response) {

		CancellationResponse ResponseObj = new CancellationResponse();
		try {
			Document doc = Jsoup.parse(Response);
			logger.info("................CancellationResponse reading Starting............");

			if (doc.getElementsByTag("cancellationResponse").attr("status").equalsIgnoreCase("Y")) {

				ResponseObj.setCancellationResponseStatus("Y");
				ResponseObj.setTourOperatorOrderNumber(doc.getElementsByTag("tourOperatorOrderNumber").text());
				ResponseObj.setCancellationNo(doc.getElementsByTag("cancellationNo").text());
				ResponseObj.setCanellationFee(doc.getElementsByTag("canellationFee").text());
				logger.info("................Cancellation NO : "+doc.getElementsByTag("cancellationNo").text());
				logger.info("................CancellationResponse reading End............");
				
			} else {

				logger.info("................CancellationResponse Status is N............");
				ResponseObj.setCancellationResponseStatus("N");
				ResponseObj.setErrorCode(doc.getElementsByTag("code").text());
				ResponseObj.setErrorDescription(doc.getElementsByTag("description").text());
				logger.info("................CancellationResponse reading End and Error Description is :" + doc.getElementsByTag("description").text() + "............");
			}

		} catch (Exception e) {

			logger.fatal("Error within the main Cancellation Response Reader is : ", e);
		}

		logger.info("................CancellationResponse Return the Response Object............");
		return ResponseObj;

	}
	
	public void buildResponse(String Path, String response) {

		try {
			org.w3c.dom.Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(response)));
			TransformerFactory tranFactory = TransformerFactory.newInstance();
			Transformer aTransformer = tranFactory.newTransformer();
			Source src = new DOMSource(doc);
			Result dest = new StreamResult(new File(Path));
			aTransformer.transform(src, dest);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
