package com.rezgateway.automation.reader.response;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.InputSource;

import com.rezgateway.automation.pojo.BookingPolicy;
import com.rezgateway.automation.pojo.HotelFees;
import com.rezgateway.automation.pojo.LookupResponse;
import com.rezgateway.automation.pojo.RateplansInfo;
import com.rezgateway.automation.pojo.Room;

public class LookupResponseReader {
	
	private static Logger logger = null;

	public LookupResponseReader() {
		logger = Logger.getLogger(ReservationResponseReader.class);
	}
	
	public LookupResponse getResponse(String Response) {

		LookupResponse ResponseObj = new LookupResponse();
		try {
			Document doc = Jsoup.parse(Response);
			if (doc.getElementsByTag("reservationresponse").attr("status").equalsIgnoreCase("Y")) {
				
				logger.info("................Start of lookup response reading..............");
				ResponseObj.setReservationStatus("Y");
				ResponseObj.setReferenceno(doc.getElementsByTag("referenceno").text().trim());
				ResponseObj.setTourOperatorName(doc.getElementsByTag("touroperatorname").text().trim());
				ResponseObj.setTourOperatorOrderNumber(doc.getElementsByTag("touroperatorordernumber").text().trim());
				ResponseObj.setHotelCode(doc.getElementsByTag("hotelCode").text().trim());
				ResponseObj.setHotelName(doc.getElementsByTag("hotelName").text().trim());
				ResponseObj.setReservationSource(doc.getElementsByTag("reservationSource").text().trim());
				ResponseObj.setReservationStatus(doc.getElementsByTag("reservationStatus").text().trim());
				ResponseObj.setCancellationNo(doc.getElementsByTag("cancellationNo").text().trim());
				ResponseObj.setCancellationFee(doc.getElementsByTag("cancellationFee").text().trim());
				ResponseObj.setCancellationTimeStamp(doc.getElementsByTag("cancellationTimeStamp").text().trim());
				ResponseObj.setConfirmationType(doc.getElementsByTag("confirmationType").text().trim());
				ResponseObj.setReservationTimeStamp(doc.getElementsByTag("reservationTimeStamp").text().trim());
				ResponseObj.setModificationTimeStamp(doc.getElementsByTag("modificationTimeStamp").text().trim());
				ResponseObj.setCheckIn(doc.getElementsByTag("checkin").text().trim());
				ResponseObj.setCheckOut(doc.getElementsByTag("checkout").text().trim());
				ResponseObj.setNoOfRooms(doc.getElementsByTag("noofrooms").text().trim());
				ResponseObj.setNoOfNights(doc.getElementsByTag("noofnights").text().trim());
				ResponseObj.setTotNoOfAdults(doc.getElementsByTag("totalnoofadults").text().trim());
				ResponseObj.setTotNoOfChildren(doc.getElementsByTag("totalnoofchildren").text().trim());
				ResponseObj.setCurrency(doc.getElementsByTag("total").attr("currency"));
				ResponseObj.setTotal(doc.getElementsByTag("total").text().trim());
				ResponseObj.setTotalTax(doc.getElementsByTag("totaltax").text().trim());
				
				ArrayList<Room> bookedRooomlist = new ArrayList<Room>();
				ArrayList<BookingPolicy> RoomBookingPolicyList = new ArrayList<BookingPolicy>();
				ArrayList<HotelFees> HotelFeeList = new ArrayList<HotelFees>();
				
				Elements roomData = doc.getElementsByTag("roomData");
				for (Element temp : roomData) {

					Room bookedRoom = new Room();
					Map<String, RateplansInfo> RatesPlanInfo = new HashMap<String, RateplansInfo>();
					RateplansInfo ratePlanObj = new RateplansInfo();

					bookedRoom.setRoomNo(temp.getElementsByTag("roomNo").text());
					bookedRoom.setRoomResNo(temp.getElementsByTag("roomResNo").text());
					bookedRoom.setRoomCode(temp.getElementsByTag("roomCode").text());
					bookedRoom.setRoomType(temp.getElementsByTag("roomType").text());
					bookedRoom.setRoomTypeID(temp.getElementsByTag("roomTypeCode").text());
					bookedRoom.setBedType(temp.getElementsByTag("bedType").text());
					bookedRoom.setBedTypeID(temp.getElementsByTag("bedTypeCode").text());

					ratePlanObj.setRatePlan(temp.getElementsByTag("ratePlan").text());
					ratePlanObj.setRatePlanCode(temp.getElementsByTag("ratePlanCode").text());
					RatesPlanInfo.put(temp.getElementsByTag("roomNo").text(), ratePlanObj);

					bookedRoom.setAdultsCount(temp.getElementsByTag("noOfAdults").text());
					bookedRoom.setChildCount(temp.getElementsByTag("noOfChildren").text());

					String[] namesList = null;
					Elements guestName = doc.getElementsByTag("guest");
					
					try {
						namesList = new String[Integer.parseInt(temp.getElementsByTag("noofadults").text()) + Integer.parseInt(temp.getElementsByTag("noofchildren").text())];
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						logger.error("Invalid Room Adults and Child Count in the Response ", e1);
					}
					int i = 0;
					for (Element temp2 : guestName) {
						try {
							String FullName = temp2.getElementsByTag("title").text() + "_" + temp2.getElementsByTag("firstname").text() + "_" + temp2.getElementsByTag("lastname").text();
							namesList[i] = FullName;
							i++;
						} catch (Exception e) {
							// TODO Auto-generated catch block
							logger.error("Error inside the Guest name iterationBlock is : ", e);
						}
					}
					
					Elements bookingPolicy = doc.getElementsByTag("bookingPolicy");
					for (Element bp : bookingPolicy) {
						
						BookingPolicy policy = new BookingPolicy();
						
						policy.setPolicyFrom(bp.getElementsByTag("policyFrom").text());
						policy.setPolicyTo(bp.getElementsByTag("policyTo").text());
						policy.setAmendmentType(bp.getElementsByTag("amendmentType").text());
						policy.setPolicyBasedOn(bp.getElementsByTag("policyBasedOn").text());
						policy.setPolicyBasedOnValue(bp.getElementsByTag("policyBasedOnValue").text());
						policy.setCancellationType(bp.getElementsByTag("cancellationType").text());
						policy.setStayDateRequirement(bp.getElementsByTag("stayDateRequirement").text());
						policy.setArrivalRange(bp.getElementsByTag("arrivalRange").text());
						policy.setArrivalRangeValue(Integer.parseInt(bp.getElementsByTag("arrivalRangeValue").text()));
						policy.setPolicyFee(bp.getElementsByTag("policyFee").text());
						policy.setNoShowBasedOn(bp.getElementsByTag("noShowBasedOn").text());
						policy.setNoShowBasedOnValue(bp.getElementsByTag("noShowBasedOnValue").text());
						policy.setNoShowPolicyFee(bp.getElementsByTag("noShowPolicyFee").text());
					
						RoomBookingPolicyList.add(policy);
					}
					
					Elements hotelFee = doc.getElementsByTag("hotelFee");
					
					for (Element hf : hotelFee) {
						
						HotelFees hfee = new HotelFees();
						
						hfee.setFeeType(hf.getElementsByTag("feeType").text());
						hfee.setFeeMethod(hf.getElementsByTag("feeMethod").text());
						hfee.setRequiredFee(hf.getElementsByTag("requiredFee").text());
						hfee.setFeeAssign(hf.getElementsByTag("feeAssign").text());
						hfee.setFeeFrequency(hf.getElementsByTag("feeFrequency").text());
						hfee.setFeeBasedOn(hf.getElementsByTag("feeBasedOn").text());
						hfee.setFeeBasedOnValue(Double.parseDouble(hf.getElementsByTag("feeBasedOnValue").text()));
						hfee.setSalesTax(Double.parseDouble(hf.getElementsByTag("salesTax").text()));
						hfee.setConditions(hf.getElementsByTag("conditions").text());
						hfee.setFeeTotal(Double.parseDouble(hf.getElementsByTag("feeTotal").text()));
									
						HotelFeeList.add(hfee);
					}
					
					bookedRoom.setLookupHotelFee(HotelFeeList);
					bookedRoom.setRoomPolicy(RoomBookingPolicyList);
					bookedRoom.setAdultsOccpancy(namesList);
					bookedRoom.setRatesPlanInfo(RatesPlanInfo);
					bookedRooomlist.add(bookedRoom);
				}
				ResponseObj.setCustomerComment(doc.getElementsByTag("customer").text().trim());	
				
				ResponseObj.setRoomlist(bookedRooomlist);
				logger.info("................End of lookup response reading.............");
			}
			else {

				logger.info("................Start of lookup response reading............");
				logger.info("................Reservation Status is " + doc.getElementsByTag("reservationResponse").attr("status") + "...........");
				ResponseObj.setReservationStatus(doc.getElementsByTag("reservationResponse").attr("status"));
				//ResponseObj.setErrorCode(doc.getElementsByTag("code").text());
				//ResponseObj.setErrorDescription(doc.getElementsByTag("description").text());
				logger.info("................Error decription : " + doc.getElementsByTag("description").text() + "...........");

			}
		}catch (Exception e) {
			logger.fatal("Error inside the main ReaderBlock is  : ", e);
		}
		return ResponseObj;
	}
	
	public void buildResponse(String Path, String response) {

		try {
			org.w3c.dom.Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(response)));
			TransformerFactory tranFactory = TransformerFactory.newInstance();
			Transformer aTransformer = tranFactory.newTransformer();
			Source src = new DOMSource(doc);
			Result dest = new StreamResult(new File(Path));
			aTransformer.transform(src, dest);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
