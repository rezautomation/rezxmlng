package com.rezgateway.automation.reader.response;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.xml.sax.InputSource;

import com.rezgateway.automation.pojo.*;

public class AvailabilityResponseReader {

	private static Logger logger = null;

	public AvailabilityResponseReader() {
		logger = Logger.getLogger(AvailabilityResponseReader.class);
	}

	AvailabilityResponse responseObj = new AvailabilityResponse();

	public AvailabilityResponse getResponse(String response) {
		try {
			Document doc = Jsoup.parse(response);
			String availbilityFlag = (doc.getElementsByTag("availabilityResponse").attr("status"));
			if (availbilityFlag.equalsIgnoreCase("Y")) {
				logger.info("................availabilityResponse reading Starting............");
				Map<String, Hotel> hotelList = new HashMap<String, Hotel>();
				responseObj.setAvailabilityStatus(availbilityFlag);
				Elements hotelListElements = doc.getElementsByTag("hotel");

				for (Element ele : hotelListElements) {

					Hotel hotel = new Hotel();
					String hotelcode = ele.getElementsByTag("hotelCode").text();
					hotel.setHotelCode(hotelcode);
					hotel.setName(ele.getElementsByTag("name").text());
					hotel.setAddress(ele.getElementsByTag("address").text());
					hotel.setCity(ele.getElementsByTag("city").text());
					hotel.setStateProvince(ele.getElementsByTag("stateProvince").text());
					hotel.setCountry(ele.getElementsByTag("country").text());
					hotel.setPostalCode(ele.getElementsByTag("postalCode").text());
					hotel.setRateCurrencyCode(ele.getElementsByTag("rateCurrencyCode").text());
					hotel.setShortDescription(ele.getElementsByTag("shortDescription").text());
					hotel.setStarRating(ele.getElementsByTag("starRating").text());
					hotel.setThumbNailUrl(ele.getElementsByTag("thumbNailUrl").text());
					hotel.setHotelUrl(ele.getElementsByTag("hotelUrl").text());
					hotel.setMaintenance(ele.getElementsByTag("maintenance").text());

					Elements hotelpolicy = ele.getElementsByTag("bookingPolicy");
					ArrayList<BookingPolicy> HotelPolicy = new ArrayList<BookingPolicy>();
					for (Element ele2 : hotelpolicy) {

						BookingPolicy hotelPolicyObj = new BookingPolicy();
						if (ele2.hasText()) {

							hotelPolicyObj.setPolicyFrom(ele2.getElementsByTag("policyFrom").text());
							hotelPolicyObj.setPolicyTo(ele2.getElementsByTag("policyTo").text());
							hotelPolicyObj.setAmendmentType(ele2.getElementsByTag("amendmentType").text());
							hotelPolicyObj.setPolicyBasedOn(ele2.getElementsByTag("policyBasedOn").text());
							hotelPolicyObj.setPolicyBasedOnValue(ele2.getElementsByTag("policyBasedOnValue").text());
							hotelPolicyObj.setCancellationType(ele2.getElementsByTag("cancellationType").text());
							hotelPolicyObj.setStayDateRequirement(ele2.getElementsByTag("stayDateRequirement").text());
							hotelPolicyObj.setArrivalRange(ele2.getElementsByTag("arrivalRange").text());
							try {
								hotelPolicyObj.setArrivalRangeValue(Integer.parseInt((ele2.getElementsByTag("arrivalRangeValue").text())));
							} catch (Exception e) {
								hotelPolicyObj.setArrivalRangeValue(0);
							}
							hotelPolicyObj.setPolicyFee(ele2.getElementsByTag("policyFee").text());
							hotelPolicyObj.setNoShowBasedOn(ele2.getElementsByTag("noShowBasedOn").text());
							hotelPolicyObj.setNoShowBasedOnValue(ele2.getElementsByTag("noShowBasedOnValue").text());
							hotelPolicyObj.setNoShowPolicyFee(ele2.getElementsByTag("noShowPolicyFee").text());
							HotelPolicy.add(hotelPolicyObj);
						} else {

							hotelPolicyObj.setPolicyFrom("ValueNotSet in the Respnse");
							hotelPolicyObj.setPolicyTo("ValueNotSet in the Respnse");
							hotelPolicyObj.setAmendmentType("ValueNotSet in the Respnse");
							hotelPolicyObj.setPolicyBasedOn("ValueNotSet in the Respnse");
							hotelPolicyObj.setPolicyBasedOnValue("ValueNotSet in the Respnse");
							hotelPolicyObj.setCancellationType("ValueNotSet in the Respnse");
							hotelPolicyObj.setStayDateRequirement("ValueNotSet in the Respnse");
							hotelPolicyObj.setArrivalRange("ValueNotSet in the Respnse");
							hotelPolicyObj.setArrivalRangeValue(0);
							hotelPolicyObj.setPolicyFee("ValueNotSet in the Respnse");
							hotelPolicyObj.setNoShowBasedOn("ValueNotSet in the Respnse");
							hotelPolicyObj.setNoShowBasedOnValue("ValueNotSet in the Respnse");
							hotelPolicyObj.setNoShowPolicyFee("ValueNotSet in the Respnse");
							HotelPolicy.add(hotelPolicyObj);
						}
					}

					hotel.setHotelBookingPolicy(HotelPolicy);
					hotel.setCountry(ele.getElementsByTag("policyDescription").text());

					Map<String, ArrayList<Room>> roomInfo = new HashMap<String, ArrayList<Room>>();

					Elements roomInfoEle = ele.getElementsByTag("roomInformation");

					for (Element ele3 : roomInfoEle) {
						String roomno = ele3.getElementsByTag("roomNo").text();
						Room roomObj = new Room();
						roomObj.setRoomNo(ele3.getElementsByTag("roomNo").text());
						roomObj.setRoomCode(ele3.getElementsByTag("roomCode").text());
						roomObj.setRoomTypeID(ele3.getElementsByTag("roomTypeCode").text());
						roomObj.setRoomType(ele3.getElementsByTag("roomType").text());
						roomObj.setRoomDescription(ele3.getElementsByTag("roomDescription").text());
						roomObj.setBedTypeID(ele3.getElementsByTag("bedTypeCode").text());
						roomObj.setBedType(ele3.getElementsByTag("bedType").text());
						roomObj.setAdultsCount(ele3.getElementsByTag("stdAdults").text());
						roomObj.setPromotionCode(ele3.getElementsByTag("promotionCode").text());
						roomObj.setConType(ele3.getElementsByTag("confirmationType").text());
						roomObj.setConfirmationConditions(ele3.getElementsByTag("confirmationConditions").text());

						Elements roomPolicyELE = ele3.getElementsByTag("roomBookingPolicy");
						ArrayList<BookingPolicy> roomPolicy = new ArrayList<BookingPolicy>();
						for (Element eleRP : roomPolicyELE) {

							BookingPolicy roomPoly = new BookingPolicy();
							roomPoly.setPolicyFrom(eleRP.getElementsByTag("policyFrom").text());
							roomPoly.setPolicyTo(eleRP.getElementsByTag("policyTo").text());
							roomPoly.setAmendmentType(eleRP.getElementsByTag("amendmentType").text());
							roomPoly.setPolicyBasedOn(eleRP.getElementsByTag("policyBasedOn").text());
							roomPoly.setPolicyBasedOnValue(eleRP.getElementsByTag("policyBasedOnValue").text());
							roomPoly.setCancellationType(eleRP.getElementsByTag("cancellationType").text());
							roomPoly.setStayDateRequirement(eleRP.getElementsByTag("stayDateRequirement").text());
							roomPoly.setArrivalRange(eleRP.getElementsByTag("arrivalRange").text());
							try {
								roomPoly.setArrivalRangeValue(Integer.parseInt((eleRP.getElementsByTag("arrivalRangeValue").text())));
							} catch (Exception e) {
								logger.error(" arrivalRangeValue is not availabile in the Response  :", e);
								roomPoly.setArrivalRangeValue(0);
							}
							roomPoly.setPolicyFee(eleRP.getElementsByTag("policyFee").text());
							roomPoly.setNoShowBasedOn(eleRP.getElementsByTag("noShowBasedOn").text());
							roomPoly.setNoShowBasedOnValue(eleRP.getElementsByTag("noShowBasedOnValue").text());
							roomPoly.setNoShowPolicyFee(eleRP.getElementsByTag("noShowPolicyFee").text());

							roomPolicy.add(roomPoly);
						}
						roomObj.setRoomPolicy(roomPolicy);

						Map<String, RateplansInfo> ratesPlanInfoMAP = new HashMap<String, RateplansInfo>();
						Elements rateInformation = ele3.getElementsByTag("rateInformation");

						for (Element ele4 : rateInformation) {

							RateplansInfo rateplanInfo = new RateplansInfo();
							String ratePlanCode = ele4.getElementsByTag("ratePlan").text();

							rateplanInfo.setRatePlanCode(ele4.getElementsByTag("ratePlanCode").text());
							rateplanInfo.setRatePlan(ele4.getElementsByTag("ratePlan").text());
							try {
								rateplanInfo.setAverageRate(Double.parseDouble(ele4.getElementsByTag("averageRate").text().replaceAll(",", "")));
							} catch (Exception e2) {
								logger.error(" rateplan averageRate  is not availabile in the Response  :", e2);
								rateplanInfo.setAverageRate(0.0);
							}
							try {
								rateplanInfo.setTotalRate(Double.parseDouble(ele4.getElementsByTag("totalRate").text().replaceAll(",", "")));
							} catch (Exception e1) {
								logger.error(" rateplan totalRate  is not availabile in the Response  :", e1);
								rateplanInfo.setTotalRate(0.0);
							}

							Elements dailyRates = ele4.getElementsByTag("nightlyRate");
							TreeMap<String, DailyRates> dailyRatesMAP = new TreeMap<String, DailyRates>();
							for (Element ele5 : dailyRates) {

								DailyRates dailyrate = new DailyRates();
								String date = ele5.getElementsByTag("nightlyRate").attr("date");
								dailyrate.setDailyCondition(ele5.getElementsByTag("nightlyRate").attr("dailyCondition"));
								dailyrate.setDate(ele5.getElementsByTag("nightlyRate").attr("date"));
								try {
									dailyrate.setStdAdultRate(Double.parseDouble(ele5.getElementsByTag("stdAdultRate").text().replaceAll(",", "")));
								} catch (Exception e) {
									logger.error(" stdAdultRate is not availabile in the Response  :", e);
									dailyrate.setStdAdultRate(0.0);
								}
								try {
									dailyrate.setAdditionalAdultRate(Double.parseDouble(ele5.getElementsByTag("additionalAdultRate").text().replaceAll(",", "")));
								} catch (Exception e) {
									logger.error(" additionalAdultRate is not availabile in the Response  :", e);
									dailyrate.setAdditionalAdultRate(0.0);
								}
								try {
									dailyrate.setTotal(Double.parseDouble(ele5.getElementsByTag("total").text().replaceAll(",", "")));
								} catch (Exception e) {
									logger.error(" total rate is not availabile in the Response  :", e);
									dailyrate.setTotal(0.0);
								}
								dailyrate.setRateCode(ele5.getElementsByTag("rateCode").text());

								dailyRatesMAP.put(date, dailyrate);
							}
							rateplanInfo.setDailyRates(dailyRatesMAP);
							Map<String, Tax> taxInfoMAP = new HashMap<String, Tax>();
							Elements taxELE = ele4.getElementsByTag("tax");
							for (Element ele6 : taxELE) {

								Tax taxinfo = new Tax();
								String taxname = ele6.getElementsByTag("tax").attr("taxName");
								taxinfo.setTaxAmount(Double.parseDouble(ele6.getElementsByTag("taxAmount").text()));
								taxInfoMAP.put(taxname, taxinfo);
							}
							rateplanInfo.setTaxInfor(taxInfoMAP);

							Elements hotelFeeELE = ele4.getElementsByTag("hotelFee");
							Map<String, HotelFees> HotelFeeInfo = new HashMap<String, HotelFees>();
							for (Element ele7 : hotelFeeELE) {

								String hotelfeeName = ele7.getElementsByTag("feeType").text();
								HotelFees hotelFee = new HotelFees();
								hotelFee.setFeeType(ele7.getElementsByTag("feeType").text());
								hotelFee.setFeeMethod(ele7.getElementsByTag("feeMethod").text());
								hotelFee.setRequiredFee(ele7.getElementsByTag("requiredFee").text());
								hotelFee.setFeeAssign(ele7.getElementsByTag("feeAssign").text());
								hotelFee.setFeeFrequency(ele7.getElementsByTag("feeFrequency").text());
								hotelFee.setFeeBasedOn(ele7.getElementsByTag("feeBasedOn").text());
								// hotelFee.setFeeBasedOnValue(!"".equals(ele7.getElementsByTag("feeBasedOnValue").text()) ? Double.parseDouble(ele7.getElementsByTag("feeBasedOnValue").text()) : 0.0);
								/*
								 * if(!"".equals(ele7.getElementsByTag("feeBasedOnValue").text())){ hotelFee.setFeeBasedOnValue(Double.parseDouble(ele7.getElementsByTag("feeBasedOnValue").text())); }
								 * else { hotelFee.setFeeBasedOnValue(0.0); logger.info(" feeBasedOnValue is not availabile :"); }
								 */

								try {
									hotelFee.setFeeBasedOnValue(Double.parseDouble(ele7.getElementsByTag("feeBasedOnValue").text().replaceAll(",", "")));
								} catch (Exception e) {
									hotelFee.setFeeBasedOnValue(0.0);
									logger.error(" feeBasedOnValue is not availabile :");
									// e.printStackTrace();
								}
								try {
									hotelFee.setSalesTax(Double.parseDouble(ele7.getElementsByTag("salesTax").text().replaceAll(",", "")));
								} catch (Exception e) {
									hotelFee.setSalesTax(0.0);
									logger.error(" sales Tax is not availabile :", e);
								}
								hotelFee.setConditions(ele7.getElementsByTag("conditions").text().replaceAll(",", ""));
								try {
									hotelFee.setFeeTotal(Double.parseDouble(ele7.getElementsByTag("feeTotal").text().replaceAll(",", "")));
								} catch (Exception e) {
									logger.error("feeTotal is not availabile :", e);
									hotelFee.setFeeTotal(0.0);

								}

								HotelFeeInfo.put(hotelfeeName, hotelFee);
							}
							rateplanInfo.setHotelFeeInfo(HotelFeeInfo);

							ratesPlanInfoMAP.put(ratePlanCode, rateplanInfo);
						}
						roomObj.setRatesPlanInfo(ratesPlanInfoMAP);
						ArrayList<Room> List = roomInfo.get(roomno.trim());

						if (null != List) {
							List.add(roomObj);
						} else {
							ArrayList<Room> newlist = new ArrayList<Room>();
							newlist.add(roomObj);
							roomInfo.put(roomno, newlist);
						}

					}
					hotel.setRoomInfo(roomInfo);
					hotelList.put(hotelcode, hotel);
				}
				responseObj.setHotelList(hotelList);
				responseObj.setHotelCount(hotelList.size());
				logger.info("hotel Count :" + hotelList.size());
				logger.info("Sucessfully Return the Availability Response");
				return responseObj;

			} else {
				logger.error("Results are not availble due to_ " + (doc.getElementsByTag("description").first().text()));
				responseObj.setErrorCode((doc.getElementsByTag("code").first().text()));
				responseObj.setErrorDescription((doc.getElementsByTag("description").first().text()));
				return responseObj;
			}

		} catch (Exception e) {
			logger.fatal("can't Read availbility Response Due to ", e);
			return responseObj;
		}

	}

	public void buildResponse(String Path, String response) {

		try {
			org.w3c.dom.Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(response)));
			TransformerFactory tranFactory = TransformerFactory.newInstance();
			Transformer aTransformer = tranFactory.newTransformer();
			Source src = new DOMSource(doc);
			Result dest = new StreamResult(new File(Path));
			aTransformer.transform(src, dest);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
