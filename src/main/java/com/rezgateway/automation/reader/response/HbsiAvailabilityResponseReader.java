package com.rezgateway.automation.reader.response;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

import com.rezgateway.automation.pojo.HbsiAvailabilityRsponse;
import com.rezgateway.automation.pojo.HbsiCancellationPolicy;
import com.rezgateway.automation.pojo.HbsiRoom;

public class HbsiAvailabilityResponseReader {
static Logger logger;
	
	public HbsiAvailabilityResponseReader() {
		logger = Logger.getLogger(HbsiAvailabilityResponseReader.class);
	}
	
	public HbsiAvailabilityRsponse responseReader(Document doc) throws JDOMException, IOException {
		logger.info("Initializing response reader with ==>" + doc.toString());
		HbsiAvailabilityRsponse response = new HbsiAvailabilityRsponse();		
				
		Element rootElem = doc.getRootElement();
		Namespace ns = Namespace.getNamespace("http://www.w3.org/2003/05/soap-envelope");
		Namespace ns1 = Namespace.getNamespace("http://www.demandmatrix.net/HBSIXML4/");
		Namespace ns2 = Namespace.getNamespace("http://www.opentravel.org/OTA/2003/05");
		
		Element body = rootElem.getChild("Body", ns);
		Element soapRequestRes = body.getChild("GetSoapRequestResponse", ns1).getChild("GetSoapRequestResult", ns1);
		String availRes = soapRequestRes.getText();
		
		SAXBuilder builder = new SAXBuilder();
		String exampleXML = availRes;
		InputStream stream = new ByteArrayInputStream(exampleXML.getBytes("UTF-8"));
		Document docReq = builder.build(stream);
		
		Element rootElemReq = docReq.getRootElement();
		Element roomStays = rootElemReq.getChild("RoomStays", ns2);
		List<Element> roomStayList = roomStays.getChildren("RoomStay", ns2);
		
		ArrayList<HbsiRoom> roomList = new ArrayList<HbsiRoom>();
		
		try {
			for (Element roomStay : roomStayList){
				Element basicPropertyInfo = roomStay.getChild("BasicPropertyInfo", ns2);
				String hotelCode = basicPropertyInfo.getAttributeValue("HotelCode");
				
				List<Element> roomRateList = roomStay.getChild("RoomRates", ns2).getChildren("RoomRate", ns2);
				HbsiRoom room = new HbsiRoom();
				
				try {
					for(Element roomRate : roomRateList){
//						HbsiRoom room = new HbsiRoom();
						String roomTypeCode = roomRate.getAttributeValue("RoomTypeCode");
						String ratePlanCode = roomRate.getAttributeValue("RatePlanCode");
						room.setRoomTypeCode(roomTypeCode);
						room.setRatePlanCode(ratePlanCode);							
						
						Element rate = roomRate.getChild("Rates", ns2).getChild("Rate", ns2);
						String totalBeforeTax = rate.getChild("Total", ns2).getAttributeValue("AmountBeforeTax");
						String totalAfterTax = rate.getChild("Total", ns2).getAttributeValue("AmountAfterTax");
						room.setTotalBeforeTax(totalBeforeTax);
						room.setTotalAfterTax(totalAfterTax);
						room.setHotelCode(hotelCode);
						
						roomList.add(room);
						response.setRoomList(roomList);
					}					
					
				} catch (Exception e) {
					logger.error("Error in reading room rate details : "+ e);
				}
				
				List<Element> guestList = roomStay.getChild("GuestCounts", ns2).getChildren("GuestCount", ns2);
				logger.info("Reading occupancy count");				
				String adultCount = null;
				String childCount = null;
		 		
		 		try {
					for (Element guest : guestList) {
						String ageCode = guest.getAttributeValue("AgeQualifyingCode");

						if ("10".equals(ageCode)) {
							adultCount = guest.getAttributeValue("Count");							
						} else if ("8".equals(ageCode)) {
							childCount = guest.getAttributeValue("Count");							
						}
						room.setAdultCount(adultCount);
						room.setChildCount(childCount);
					}					
				} catch (Exception e) {
					logger.error("Error in reading occupancy details");
				}
		 		
		 		List<Element> cancelPenaltyList = roomStay.getChild("CancelPenalties", ns2).getChildren("CancelPenalty", ns2);
		 		ArrayList<HbsiCancellationPolicy> cpList = new ArrayList<HbsiCancellationPolicy>();
		 		logger.info("Reading cancellation policy details");
		 		try {
		 			for (Element cancelPenalty : cancelPenaltyList){
		 				String policyCode = cancelPenalty.getAttributeValue("PolicyCode");
		 				String nonRefundable = cancelPenalty.getAttributeValue("NonRefundable");
		 				String deadline = cancelPenalty.getChild("Deadline", ns2).getAttributeValue("AbsoluteDeadline");
		 				List<Attribute> attributes = cancelPenalty.getChild("AmountPercent", ns2).getAttributes();
		 				
		 				HbsiCancellationPolicy cp = new HbsiCancellationPolicy();
		 				cp.setPolicyCode(policyCode);
		 				cp.setNonRefundable(nonRefundable);
		 				cp.setDeadLine(deadline);
		 				
		 				for(Attribute text : attributes){
		 					String name = text.getName();
		 					cp.setAmountPercentUnit(name);
		 					cp.setAmountPercentAmnt(text.getValue());
		 				}			
		 				cpList.add(cp);
		 				room.setCp(cpList);		 				
		 			}
					
				} catch (Exception e) {
					logger.error("Error in reading cancellation policy details");
				}		 		
				
			}
		} catch (Exception e) {
			logger.error("Error in reading room stay details : "+ e);
		}
		
		logger.info("Reading room stay details");
				
		return response;
	}

}
