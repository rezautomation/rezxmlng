package com.rezgateway.automation.reader.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;

import com.rezgateway.automation.pojo.SynxisAvailabilityResponse;
import com.rezgateway.automation.pojo.SynxisCancellationPolicy;
import com.rezgateway.automation.pojo.SynxisRoom;

public class SynxisAvailabilityResponseReader {
	
	static Logger logger;
	
	public SynxisAvailabilityResponseReader() {
		logger = Logger.getLogger(SynxisAvailabilityResponseReader.class);
	}
	
	public SynxisAvailabilityResponse responseReader(Document doc) {
		
		logger.info("Initializing response reader with ==>" + doc.toString());
		SynxisAvailabilityResponse response = new SynxisAvailabilityResponse();		
				
		Element rootElem = doc.getRootElement();
		
		Namespace ns = Namespace.getNamespace("http://www.w3.org/2003/05/soap-envelope");
		Namespace ns1 = Namespace.getNamespace("http://www.opentravel.org/OTA/2003/05");
		
		Element body = rootElem.getChild("Body", ns);
		Element OTA_HotelAvailRS = body.getChild("OTA_HotelAvailRS", ns1);
		String echoToken = OTA_HotelAvailRS.getAttributeValue("EchoToken");
		response.setEchoToken(echoToken);	
				
		Element roomStay = OTA_HotelAvailRS.getChild("RoomStays", ns1).getChild("RoomStay", ns1);
		
		logger.info("Reading hotel details");
		
		Element basicPropertyInfo = roomStay.getChild("BasicPropertyInfo", ns1);
		String hotelCode = basicPropertyInfo.getAttributeValue("HotelCode");
		String hotelName = basicPropertyInfo.getAttributeValue("HotelName");
		response.setHotelCode(hotelCode);
		response.setHotelName(hotelName);
		
		logger.info("Reading room details");
		
		List<Element> roomTypes = roomStay.getChild("RoomTypes", ns1).getChildren("RoomType", ns1);
		Map<String, String[]> roomTypeMap = new HashMap<String, String[]>();
		
		try {
			for (Element roomType : roomTypes) {
				String roomTypeCode = roomType.getAttributeValue("RoomTypeCode");
				String noOfUnits = roomType.getAttributeValue("NumberOfUnits");

				Element roomDescription = roomType.getChild("RoomDescription", ns1);
				String roomTypeName = roomDescription.getAttributeValue("Name");
				String roomDesc = roomDescription.getChild("Text", ns1).getValue();

				roomTypeMap.put(roomTypeCode, new String[] { noOfUnits,	roomTypeName, roomDesc });
				response.setRoomTypes(roomTypeMap);
			}
		} catch (Exception e) {
			logger.error("Room details not read properly" + e);
		}
		
		logger.info("Reading rate plan details");
		
		List<Element> ratePlans = roomStay.getChild("RatePlans", ns1).getChildren("RatePlan", ns1);
		Map<String, String[]> ratePlanMap = new HashMap<String, String[]>();
		try {
			for (Element ratePlan : ratePlans) {
				String ratePlanCode = ratePlan.getAttributeValue("RatePlanCode");
				String ratePlanName = ratePlan.getAttributeValue("RatePlanName");

				Element rateDescription = ratePlan.getChild("RatePlanDescription", ns1);
				String rateDesc = rateDescription.getChild("Text", ns1).getValue();

				ratePlanMap.put(ratePlanCode, new String[] { ratePlanName, rateDesc });
				response.setRatePlans(ratePlanMap);
			}
		} catch (Exception e) {
			logger.error("Rate plan details not read properly" + e);
		}
		
		List<Element> roomRateList = roomStay.getChild("RoomRates", ns1).getChildren("RoomRate", ns1);
		ArrayList<SynxisRoom> roomList = new ArrayList<SynxisRoom>();		
		
		logger.info("Reading rate details");
		
		try{
			for(Element roomRate : roomRateList){
				SynxisRoom room = new SynxisRoom();
				String roomTypeCode = roomRate.getAttributeValue("RoomTypeCode");
				String ratePlanCode = roomRate.getAttributeValue("RatePlanCode");
				room.setRoomTypeCode(roomTypeCode);
				room.setRatePlanCode(ratePlanCode);	
			
				roomList.add(room);
				response.setRoomList(roomList);			
			
				Element rate = roomRate.getChild("Rates", ns1).getChild("Rate", ns1);
			
				Element total = rate.getChild("Total", ns1);
				String totalAfterTax = total.getAttributeValue("AmountAfterTax");
				room.setTotalAfterTax(totalAfterTax);
						
				Element cancellationPenaltyList = rate.getChild("CancelPolicies", ns1);
				List<Element> penalties = cancellationPenaltyList.getChildren();
			
				logger.info("Reading cancellation policy details");
				try {
					for (Element penalty : penalties) {
						SynxisCancellationPolicy cancellationPolicy = new SynxisCancellationPolicy();
						ArrayList<SynxisCancellationPolicy> cpList = new ArrayList<SynxisCancellationPolicy>();
						String policyCode = penalty.getAttributeValue("PolicyCode");
						cancellationPolicy.setPolicyCode(policyCode);

						String penaltyDesc = penalty.getChild("PenaltyDescription", ns1).getChild("Text", ns1).getValue();
						cancellationPolicy.setPenltyDesc(penaltyDesc);

						String amount = penalty.getChild("AmountPercent", ns1).getAttributeValue("Amount");
						cancellationPolicy.setAmount(amount);
						String taxInclusive = penalty.getChild("AmountPercent", ns1).getAttributeValue("TaxInclusive");
						cancellationPolicy.setIsTaxInclusive(taxInclusive);

						Element deadline = penalty.getChild("Deadline", ns1);

						if (deadline != null) {
							String deadLine = deadline.getAttributeValue("AbsoluteDeadline");
							String offsetTimeUnit = deadline.getAttributeValue("OffsetTimeUnit");
							String offsetUnitMultiplier = deadline.getAttributeValue("OffsetUnitMultiplier");
							String offsetDropTime = deadline.getAttributeValue("OffsetDropTime");

							cancellationPolicy.setDeadLine(deadLine);
							cancellationPolicy.setOffsetTimeUnit(offsetTimeUnit);
							cancellationPolicy.setOffsetUnitMultiplier(offsetUnitMultiplier);
							cancellationPolicy.setOffsetDropTime(offsetDropTime);
						} 
						else {
							cancellationPolicy.setDeadLine("null");
							cancellationPolicy.setOffsetTimeUnit("null");
							cancellationPolicy.setOffsetUnitMultiplier("null");
							cancellationPolicy.setOffsetDropTime("null");
						}
						cpList.add(cancellationPolicy);
						room.setCp(cpList);
					}
			} catch (Exception e) {
				logger.error("Error in reading cancellation policy details : "+ e);
			}				
		}
		}
		catch(Exception e){
			logger.error("Error in reading rate details : "+ e);
		}
		
		return response;		
	}

}
