package com.rezgateway.automation.utills;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

public class NodeListBuilder {
	private static Logger logger ;
	
	public NodeListBuilder() {
		logger = Logger.getLogger(NodeListBuilder.class);
	}

	public NodeList getNodeList(String tagname) {

		try {
			logger.debug("==== Starting node list processer =========");
			logger.debug(tagname);
			InputStream st = new FileInputStream(new File("Resources/Pages/" + tagname + ".xml"));
			DocumentBuilderFactory dbFacory = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbFacory.newDocumentBuilder();
			Document doc = db.parse(st);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("element");
			logger.debug(doc.getDocumentElement().getNodeName());
			logger.debug("Node list processer completed returning ---> " + nList);
			return nList;

		} catch (Exception e) {
			logger.fatal("Error in Node list processer ", e);
			return null;
		}
	}

}
